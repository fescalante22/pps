﻿using Abp.Web.Mvc.Views;

namespace DonacionMedicamentos.Web.Views
{
    public abstract class DonacionMedicamentosWebViewPageBase : DonacionMedicamentosWebViewPageBase<dynamic>
    {

    }

    public abstract class DonacionMedicamentosWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected DonacionMedicamentosWebViewPageBase()
        {
            LocalizationSourceName = DonacionMedicamentosConsts.LocalizationSourceName;
        }
    }
}