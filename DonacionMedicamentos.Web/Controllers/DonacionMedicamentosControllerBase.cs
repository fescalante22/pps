﻿using Abp.Web.Mvc.Controllers;

namespace DonacionMedicamentos.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class DonacionMedicamentosControllerBase : AbpController
    {
        protected DonacionMedicamentosControllerBase()
        {
            LocalizationSourceName = DonacionMedicamentosConsts.LocalizationSourceName;
        }
    }
}