﻿using DonacionMedicamentos.Drugs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DonacionMedicamentos.Web.Controllers
{
    public class DrugController : Controller
    {
        private readonly IDrugAppService _drugAppService;

        public DrugController(IDrugAppService drugAppService)
        {
            this._drugAppService = drugAppService;
        }

        // GET: Drug
        public ActionResult Index()
        {
            return View();
        }

        // GET: Drug/GetAll
        public ActionResult GetAll()
        {
            return Json(new { data = this._drugAppService.GetAll()}, JsonRequestBehavior.AllowGet);
        }


    }
}