﻿using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Web.Models;
using System.Web;
using System.Web.Mvc;

namespace DonacionMedicamentos.Web.Controllers
{
    public class HomeController : DonacionMedicamentosControllerBase
    {
        public HomeController() { }

        public ActionResult Index()
        {
            ActionResult _view;
            HttpCookie myCookie = Request.Cookies["UserData"];
            if (Request.IsAuthenticated)
            {
                if (myCookie["User"] == "rol_admin")
                {
                    _view = RedirectToAction("DashboardAdmin", "Admin");
                }
                else if (myCookie["User"] == "rol_person")
                {
                    _view = RedirectToAction("DashboardPerson", "Person");
                }
                else
                {
                    _view = RedirectToAction("DashboardIntermediary", "Intermediary");
                }
                return _view;
            }
            return View();
        }

    }

}