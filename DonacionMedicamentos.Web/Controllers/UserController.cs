﻿using Abp.UI;
using DonacionMedicamentos.Users;
using DonacionMedicamentos.Personas;
using DonacionMedicamentos.Users.DTO;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DonacionMedicamentos.Personas.DTO;
using DonacionMedicamentos.Intermediarios.DTO;
using DonacionMedicamentos.Intermediarios;
using System.Security.Claims;
using System.Net;

namespace DonacionMedicamentos.Web.Controllers
{
    public class UserController : DonacionMedicamentosControllerBase
    {
        private readonly IUserAppService _userAppService;
        private readonly IPersonAppService _personAppService;
        private readonly IIntermediaryAppService _intermediaryAppService;

        public UserController(
            UserAppService userAppService, 
            PersonAppService personAppService, 
            IntermediaryAppService intermediaryAppService)
        {
            _userAppService = userAppService;
            _personAppService = personAppService;
            _intermediaryAppService = intermediaryAppService;
        }

        // GET: User/Login
        [Route("Person")]
        public ActionResult Login()
        {
            ActionResult _view;
            HttpCookie myCookie = Request.Cookies["UserData"];
            if (Request.IsAuthenticated)
            {
                if (myCookie["User"] == "rol_admin")
                {
                    _view = RedirectToAction("DashboardAdmin", "Admin");
                }
                else if (myCookie["User"] == "rol_person")
                {
                    _view = RedirectToAction("DashboardPerson", "Person");
                }
                else
                {
                    _view = RedirectToAction("DashboardIntermediary", "Intermediary");
                }
                return _view;
            }
            return View();
        }


        // POST: User/Login
        [HttpPost]
        public ActionResult Login(FormCollection collection)
        {
            try
            {
                string pAccount = Request.Form["Account"];
                string pPassword = Request.Form["Contraseña"];
                string pRol = Request.Form["Rol"];
                bool rememberMe = Request.Form["RememberMe"].Contains("true");
                

                LoginUserInput logindto = new LoginUserInput() { Account = pAccount, Password = pPassword, RememberMe = rememberMe };
                _userAppService.LoginUser(logindto);
                
                string roleAuth="";
                string idAuth="";
                HttpCookie myCookie = new HttpCookie("UserData");
                switch (pRol)
                {
                    case "rol_person":
                        PersonDTO person = new PersonDTO { Account = logindto.Account };
                        person = _personAppService.GetByAccount(person);
                        myCookie["Id"] = person.Id.ToString();
                        myCookie["User"] = "rol_person";
                        myCookie["NombreCompleto"] = person.Nombre + " " + person.Apellido.Substring(0, 1) + ".";
                        Response.Cookies.Add(myCookie);
                        roleAuth = "Person";
                        idAuth = person.Id.ToString();
                        break;
                    case "rol_intermediary":
                        IntermediaryDTO intermediary = new IntermediaryDTO { Account = logindto.Account };
                        intermediary = this._intermediaryAppService.GetByAccount(intermediary);
                        myCookie["Id"] = intermediary.Id.ToString();
                        myCookie["NombreCompleto"] = intermediary.Nombre;
                        myCookie["User"] = "rol_intermediary";
                        Response.Cookies.Add(myCookie);
                        roleAuth = "Intermediary";
                        idAuth = intermediary.Id.ToString();
                        break;
                }
                FormsAuthentication.SetAuthCookie(idAuth, false);
                var authTicket = new FormsAuthenticationTicket(1, idAuth, DateTime.Now, DateTime.Now.AddMinutes(20), logindto.RememberMe, roleAuth);
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                HttpContext.Response.Cookies.Add(authCookie);

                return RedirectToAction("Index","Home");
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
                return View("Login");
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Crear(FormCollection collection)
        {  
            string pAccount = Request.Form["Id"];
            string pemail = Request.Form["Email"];
            string pPassword = Request.Form["Contraseña"];

            CreateUserInput user = new CreateUserInput { Account = pAccount, Password = pPassword, Email = pemail };
            _userAppService.CreateUser(user);

            try
            {
                return RedirectToRoute(new
                {
                    controller = "Home",
                    action = "Index",
                });
            }
            catch
            {
                return RedirectToRoute(new
                {
                    controller = "Home",
                    action = "Index",
                });
            }
        }

        // GET: User/RecoverPass
        public ActionResult RecoverPass()
        {
            return View("RecoverPass");
        }

        // POST: User/RecoverPass
        [HttpPost]
        public ActionResult RecoverPass(FormCollection collection)
        {
            RecoverPassUserInput user = new RecoverPassUserInput
            {
                Account = Request.Form["Account"],
            };

            _userAppService.RecoverPassUser(user);

            return RedirectToRoute(new
            {
                controller = "User",
                action = "Login",
            });
        }

        // GET: User/ChangePass
        [Authorize(Roles = "Person, Intermediary, Admin")]
        public ActionResult ChangePass()
        {
            ChangePassUserInput user = new ChangePassUserInput { Id = int.Parse(Request.Cookies["UserData"]["Id"]) };
            return View("ChangePass", user);
        }

        // POST: User/ChangePass
        [HttpPost]
        [Authorize(Roles = "Person, Intermediary, Admin")]
        public ActionResult ChangePass(ChangePassUserInput model)
        {
            try
            {
                if (model.PasswordNueva == model.RepeatPasswordNueva)
                {
                    _userAppService.ChangePassUser(model);
                }
                else
                {
                    throw new UserFriendlyException("Las nuevas contraseñas son distintas");
                }
                return RedirectToAction("Index", "Home");
            }
            catch (UserFriendlyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ChangePass", model);
            }
            catch(Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ChangePass", model);
            }
        }
    }
}

