﻿using Abp.ObjectMapping;
using Abp.UI;
using DonacionMedicamentos.Comments;
using DonacionMedicamentos.Donations.DTO;
using DonacionMedicamentos.DonationStates;
using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Intermediarios.DTO;
using DonacionMedicamentos.Locations.DTO;
using DonacionMedicamentos.Phones;
using DonacionMedicamentos.Requests.DTO;
using DonacionMedicamentos.RequestStates;
using DonacionMedicamentos.Web.Models;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace DonacionMedicamentos.Web.Controllers
{
    public class IntermediaryController : Controller
    {
        private readonly IIntermediaryAppService _intermediaryAppService;
        private readonly IObjectMapper _objectMapper;

        public IntermediaryController(
            IIntermediaryAppService intermediaryAppService,
            IObjectMapper objectMapper)
        {
            _intermediaryAppService = intermediaryAppService;
            _objectMapper = objectMapper;
        }

        // GET: Intermediary
        public ActionResult Index()
        {
            HttpCookie myCookie = Request.Cookies["UserData"];
            if (Request.IsAuthenticated)
            {
                if (myCookie["User"] == "rol_intermediary")
                {
                    return RedirectToAction("DashboardIntermediary", "Intermediary");
                }
            }
            return RedirectToAction("Login", "User");
        }

        // GET: Intermediary/Edit
        [Authorize(Roles = "Intermediary")]
        public ActionResult Edit()
        {
            HttpCookie myCookie = Request.Cookies["UserData"];
            IntermediaryDTO intermediary = new IntermediaryDTO { Id = int.Parse(myCookie["Id"]), Account = "0" };
            intermediary = this._intermediaryAppService.Get(intermediary);
            return View("Edit", intermediary);
        }

        // POST: Intermediary/Edit
        [HttpPost]
        [Authorize(Roles = "Intermediary")]
        public virtual ActionResult Edit(IntermediaryDTO model)
        {
            try
            {
                int cantidadTelefonos = int.Parse(Request.Form["CantidadTelefonos"]);

                ///Controlo que al menos tenga un telefono
                if (cantidadTelefonos > 0)
                {
                    ///Obtengo la lista de los telefonos
                    List<PhoneDTO> pTelefonos = new List<PhoneDTO>();
                    for (int i = 0; i < cantidadTelefonos; i++)
                    {
                        int codArea = int.Parse(Request.Form["codArea" + i]);
                        int telefono = int.Parse(Request.Form["telefono" + i]);
                        PhoneDTO phone = new PhoneDTO { CodigoArea = codArea, Numero = telefono };
                        pTelefonos.Add(phone);
                    }
                    model.Telefonos = pTelefonos;
                }
                else
                {
                    throw new UserFriendlyException("Debe registrarse al menos un telefono");
                }

                model.Ciudad = new CityDTO { Id = int.Parse(Request.Form["Ciudad"]) };
                model.Id = int.Parse(Request.Cookies["UserData"]["Id"]);

                UpdateIntermediaryInput intermediaryInput = this._objectMapper.Map<UpdateIntermediaryInput>(model);
                this._intermediaryAppService.UpdateIntermediary(intermediaryInput);

                //Actualizamos las cookies
                Response.Cookies.Clear();
                HttpCookie myCookie = new HttpCookie("UserData");
                myCookie["Id"] = model.Id.ToString();
                myCookie["NombreCompleto"] = model.Nombre;
                myCookie["User"] = "rol_intermediary";
                Response.Cookies.Add(myCookie);

                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("Edit", model);
            }
        }

        // GET: Intermediary/Dashboard
        [Authorize(Roles = "Intermediary")]
        public ActionResult DashboardIntermediary()
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"].ToString();
                TempData.Remove("ErrorMessage");
            }
            if (TempData["InfoMessage"] != null)
            {
                ViewBag.InfoMessage = TempData["InfoMessage"].ToString();
                TempData.Remove("InfoMessage");
            }
            HttpCookie myCookie = Request.Cookies["UserData"];
            IntermediaryDTO intermediary = new IntermediaryDTO { Id = int.Parse(myCookie["Id"]), Account = "0" };
            var model = new ListIntermediaryModel(_intermediaryAppService.GetAllRequests(intermediary), _intermediaryAppService.GetAllDonations(intermediary));
            return View("DashboardIntermediary", model);
        }

        // POST: Intermediary/Dashboard
        [HttpPost]
        [Authorize(Roles = "Intermediary")]
        public ActionResult DashboardIntermediary(FormCollection form)
        {
            HttpCookie myCookie = Request.Cookies["UserData"];
            IntermediaryDTO intermediary = new IntermediaryDTO { Id = int.Parse(myCookie["Id"]), Account = "0" };
            string check = Request.Form["checkPropias"];
            if (check == "true")
            {
                var model = new ListIntermediaryModel(_intermediaryAppService.GetAllRequests(intermediary), _intermediaryAppService.GetAllDonations(intermediary));
                model.requests = model.requests.FindAll(x => !(x.Intermediario == null) && (x.Intermediario.Id.ToString() == myCookie["Id"]));
                model.donations = model.donations.FindAll(x => !(x.Intermediario == null) && (x.Intermediario.Id.ToString() == myCookie["Id"]));
                ViewBag.IsChecked = "t";
                return View("DashboardIntermediary", model);
            }
            else
            {
                var model = new ListIntermediaryModel(_intermediaryAppService.GetAllRequests(intermediary), _intermediaryAppService.GetAllDonations(intermediary));
                ViewBag.IsChecked = "f";
                return View("DashboardIntermediary", model);
            }
        }

        // GET: Intermediary/GetRequest/5
        [Authorize(Roles = "Intermediary")]
        public ActionResult GetRequest(int id)
        {
            IntermediaryDTO pIntermediary = this._intermediaryAppService.Get(new IntermediaryDTO { Id = int.Parse(Request.Cookies["UserData"]["Id"]), Account = "0" });
            GetRequestsInput request = new GetRequestsInput { Id = id, Intermediario = pIntermediary };
            GetRequestsOutput model = this._intermediaryAppService.GetRequest(request);
            return View("~/Views/Request/show.cshtml", model);
        }

        // POST: Intermediary/AddComment/5
        [Authorize(Roles = "Intermediary")]
        [HttpPost]
        public virtual ActionResult AddComment(FormCollection collection,int id)
        {
            string comment = (Request.Form["comment"]);
            this._intermediaryAppService.AddCommentToRequest(new AddCommentToRequestInput { Id = id, Comment = new CommentDTO { Detalle = comment,Fecha=DateTime.Now } });
            return RedirectToAction("GetRequest", new { id = id });
        }

        // GET: Intermediary/ApproveRequest/5
        [Authorize(Roles = "Intermediary")]
        public virtual ActionResult ApproveRequest(int id)
        {
            IntermediaryDTO intermediary = new IntermediaryDTO { Id = int.Parse(Request.Cookies["UserData"]["Id"]), Account = "0" };
            this._intermediaryAppService.ApproveRequest(new ApproveRequestInput { Id = id, Intermediario = intermediary });
            return RedirectToAction("GetRequest", new { id = id });
        }

        // GET: Intermediary/GetDonation
        [Authorize(Roles = "Intermediary")]
        public ActionResult GetDonation(int id)
        {
            IntermediaryDTO pIntermediary = this._intermediaryAppService.Get(new IntermediaryDTO { Id = int.Parse(Request.Cookies["UserData"]["Id"]), Account = "0" });
            GetDonationsInput donation = new GetDonationsInput { Id = id, Intermediario = pIntermediary };
            GetDonationsOutput model = this._intermediaryAppService.GetDonation(donation);
            return View("~/Views/Donation/Show.cshtml", model);
        }

        // GET: Intermediary/ApproveDonation/5
        [Authorize(Roles = "Intermediary")]
        public virtual ActionResult ApproveDonation(int id)
        {
            IntermediaryDTO intermediary = new IntermediaryDTO { Id = int.Parse(Request.Cookies["UserData"]["Id"]), Account = "0" };
            this._intermediaryAppService.ApproveDonation(new ApproveDonationInput { Id = id, Intermediario = intermediary });
            return RedirectToAction("GetDonation", new { id = id });
        }


        // GET: Intermediary/ChangeRequestState
        [Authorize(Roles = "Intermediary")]
        public virtual ActionResult ChangeRequestState(FormCollection collection)
        {
            int idRequest = int.Parse(collection["idRequest"]);
            int newState = int.Parse(collection["RequestState"]);
            try
            {
                this._intermediaryAppService.ChangeRequestState(new ChangeRequestStateInput { Id = idRequest, Estado = newState });
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"]= ex.Message;
            }
            return RedirectToAction("DashboardIntermediary");
        }


        // GET: Intermediary/ApproveRequest/5
        [Authorize(Roles = "Intermediary")]
        public virtual ActionResult ChangeDonationState(FormCollection collection)
        {
            int idDonation = int.Parse(collection["idDonation"]);
            int newState = int.Parse(collection["DonationState"]);
            try
            {
                this._intermediaryAppService.ChangeDonationState(new ChangeDonationStateInput { Id = idDonation, Estado = newState });
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
            }
            return RedirectToAction("DashboardIntermediary");
        }

        // GET: Intermediary/CancelRequest/5
        [Authorize(Roles = "Intermediary")]
        public virtual ActionResult CancelRequest(int id)
        {
            this._intermediaryAppService.ChangeRequestState(new ChangeRequestStateInput { Id = id,  Estado = (int)RequestState.Cancelada });
            TempData["InfoMessage"] = "La solicitud fue cancelada satisfactoriamente";
            return RedirectToAction("Index");
        }

        // GET: Intermediary/CancelDonation/5
        [Authorize(Roles = "Intermediary")]
        public virtual ActionResult CancelDonation(int id)
        {
            this._intermediaryAppService.ChangeDonationState(new ChangeDonationStateInput { Id = id, Estado = (int)DonationState.Cancelada });
            TempData["InfoMessage"] = "La donacion fue cancelada satisfactoriamente";
            return RedirectToAction("Index");
        }
    }
}
           
        