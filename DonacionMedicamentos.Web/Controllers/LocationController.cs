﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DonacionMedicamentos.Locations;
using DonacionMedicamentos.Locations.DTO;

namespace DonacionMedicamentos.Web.Controllers
{
    public class LocationController : Controller
    {
        private readonly LocationAppService _locationAppService;

        public LocationController(LocationAppService locationAppService)
        {
            this._locationAppService = locationAppService;
        }
        
        // GET: Location
        public ActionResult Index()
        {
            return View();
        }

        // GET: Location/States
        public ActionResult States()
        {
            return Json(this._locationAppService.GetAllStates(), JsonRequestBehavior.AllowGet); 
        }


        // POST: Location/Cities
        [HttpPost]
        public ActionResult Cities(int idState)
        {
            StateDTO pState = new StateDTO {Id = idState};
            return Json(this._locationAppService.GetAllCities(pState), JsonRequestBehavior.AllowGet);
        }
    }
}