﻿using Abp.Authorization;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Mvc.Authorization;
using DonacionMedicamentos.Admins;
using DonacionMedicamentos.Admins.DTO;
using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Intermediarios.DTO;
using DonacionMedicamentos.Locations.DTO;
using DonacionMedicamentos.Phones;
using DonacionMedicamentos.Users;
using DonacionMedicamentos.Users.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
namespace DonacionMedicamentos.Web.Controllers
{
    public class AdminController : Controller
    {
        private readonly IAdminAppService _adminAppService;
        private readonly IUserAppService _userAppService;
        private readonly IIntermediaryAppService _intermediaryAppService;
        private readonly IAbpSession _session;
        private readonly IPermissionChecker _permissionChecker;
        private readonly IPermissionManager _permissionManager;
        

        public AdminController(
            IAdminAppService adminAppService,
            IUserAppService userAppService,
            IIntermediaryAppService intermediaryAppService,
            IAbpSession session,
            IPermissionChecker permissionChecker,
            IPermissionManager permissionManager)
        {
            _adminAppService = adminAppService;
            _userAppService = userAppService;
            _intermediaryAppService = intermediaryAppService;
            _session = session;
            _permissionChecker = permissionChecker;
            _permissionManager = permissionManager;
        }

        // GET: Admin
        public ActionResult Index()
        {
            HttpCookie myCookie = Request.Cookies["UserData"];
            if (Request.IsAuthenticated)
            {
                if (myCookie["User"] == "rol_admin")
                {
                    return RedirectToAction("DashboardAdmin", "Admin");
                }
            }
            return RedirectToAction("Login", "Admin");
        }

        // GET: Admin/Login
        public ActionResult Login()
        {
            HttpCookie myCookie = Request.Cookies["UserData"];
            if (Request.IsAuthenticated)
            {
                 return RedirectToAction("DashboardAdmin", "Admin");
            }
            return View();
        }

        // POST: Admin/Login
        [HttpPost]
        public ActionResult Login(FormCollection collection)
        {
            try
            {
                LoginUserInput logindto = new LoginUserInput() { Account = Request.Form["Account"], Password = Request.Form["Contraseña"], RememberMe = true };
                if (_userAppService.LoginUser(logindto))
                {
                    //int idUser;
                    HttpCookie myCookie = new HttpCookie("UserData");
                    AdminDTO admin = new AdminDTO { Account = logindto.Account };
                    admin = this._adminAppService.GetByAccount(admin);
                    
                    myCookie["Id"] = admin.Id.ToString();
                    myCookie["NombreCompleto"] = admin.Nombre;
                    myCookie["User"] = "rol_admin";
                    Response.Cookies.Add(myCookie);                   

                    FormsAuthentication.SetAuthCookie(admin.Id.ToString(), false);
                    var authTicket = new FormsAuthenticationTicket(1, admin.Id.ToString(), DateTime.Now, DateTime.Now.AddMinutes(20), logindto.RememberMe,"Admin");
                    string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                    var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    HttpContext.Response.Cookies.Add(authCookie);
                }
                else
                {
                    throw new Exception("Usuario o contraseña incorrecta");
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
                return View("Login");
            }
            return RedirectToAction("DashboardAdmin", "Admin");

        }

        // GET: Admin/CreateIntermediary
        [Authorize(Roles = "Admin")]
        public ActionResult CreateIntermediary()
        {
            return View("RegisterIntermediary",new RegisterIntermediaryInput());
        }

        // POST: Admin/CreateIntermediary
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult CreateIntermediary(RegisterIntermediaryInput model)
        {
            try
            { 
                int idProvincia = int.Parse(Request.Form["Provincia"]);
                int idCiudad = int.Parse(Request.Form["Ciudad"]);
                int cantidadTelefonos = int.Parse(Request.Form["CantidadTelefonos"]);

                List<PhoneDTO> pTelefonos = new List<PhoneDTO>();
                for (int i = 0; i < cantidadTelefonos; i++)
                {
                    int codArea = int.Parse(Request.Form["codArea" + i]);
                    int telefono = int.Parse(Request.Form["telefono" + i]);

                    PhoneDTO phone = new PhoneDTO { CodigoArea = codArea, Numero = telefono };
                    pTelefonos.Add(phone);
                }

                if(cantidadTelefonos<1)
                {
                    throw new UserFriendlyException("Se debe registrar al menos un teléfono.");
                }

                if (model.Password != model.RepeatPassword)
                {
                    throw new UserFriendlyException("Las contraseñas no son iguales");
                }

                model.Telefonos = pTelefonos;
                model.Ciudad = new CityDTO { Id = idCiudad, Provincia = new StateDTO { Id = idProvincia } };

                _adminAppService.RegisterIntermediary(model);
                return RedirectToAction("DashboardAdmin");
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
                return View("RegisterIntermediary", model);
            }
        }


        // GET: Admin/DashboardAdmin
        [Authorize(Roles = "Admin")]
        public ActionResult DashboardAdmin()
        {
            var intermediaries = this._adminAppService.GetAllIntermediaries();
            return View("DashboardAdmin", intermediaries);
        }

        // POST: Admin/DashboardAdmin
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult DashboardAdmin(FormCollection collection)
        {
            _adminAppService.UnRegisterIntermediary(new UnregisterIntermediaryInput { Id = Convert.ToInt32(collection["idToDelete"]) });
            return RedirectToAction("DashboardAdmin");
        }
    }
}
