﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using DonacionMedicamentos.Personas;
using DonacionMedicamentos.Phones;
using DonacionMedicamentos.Personas.DTO;
using DonacionMedicamentos.Web.Models;
using DonacionMedicamentos.Locations.DTO;
using DonacionMedicamentos.Drugs.DTO;
using System.IO;
using Abp.UI;
using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Requests.DTO;
using Abp.ObjectMapping;
using DonacionMedicamentos.Donations.DTO;

namespace DonacionMedicamentos.Web.Controllers
{
    public class PersonController : DonacionMedicamentosControllerBase
    {
        private readonly PersonAppService _personAppService;
        private readonly IntermediaryAppService _intermediaryAppService;
        private readonly IObjectMapper _objectMapper;

        public PersonController(PersonAppService personAppService, IntermediaryAppService intermediaryAppService,
             IObjectMapper objectMapper)
        {
            _personAppService = personAppService;
            _intermediaryAppService = intermediaryAppService;
            _objectMapper = objectMapper;
        }

        // GET: Person
        public ActionResult Index()
        {
            HttpCookie myCookie = Request.Cookies["UserData"];
            if (Request.IsAuthenticated)
            {
                if (myCookie["User"] == "rol_person")
                {
                    return RedirectToAction("DashboardPerson", "Person");
                }
            }
            return RedirectToAction("Login", "User");
        }

        // GET: Person/Create
        public ActionResult Create()
        {
            CreatePersonInput model = new CreatePersonInput();
            return View("RegisterPerson",model);
        }

        // POST: Person/Create
        [HttpPost]
        public ActionResult Create(CreatePersonInput model, FormCollection collection)
        {
            try
            {
                int idProvincia = int.Parse(Request.Form["Provincia"]);
                int idCiudad = int.Parse(Request.Form["Ciudad"]);
                int cantidadTelefonos = int.Parse(Request.Form["CantidadTelefonos"]);

                if(cantidadTelefonos<1)
                {
                    throw new UserFriendlyException("Se debe ingresar más de un telefono");
                }

                List<PhoneDTO> pTelefonos = new List<PhoneDTO>();
                for (int i=0; i < cantidadTelefonos; i++)
                {
                    int codArea = int.Parse(Request.Form["codArea"+i]);
                    int telefono = int.Parse(Request.Form["telefono"+i]);
                    PhoneDTO phone = new PhoneDTO { CodigoArea = codArea, Numero = telefono };
                    pTelefonos.Add(phone);
                }
               
                if (model.Password != model.RepeatPassword)
                {
                    throw new UserFriendlyException("Las contraseñas no son iguales");
                }

                model.Telefonos = pTelefonos;
                model.Ciudad = new CityDTO { Id = idCiudad, Provincia = new StateDTO { Id = idProvincia } };

                _personAppService.CreatePerson(model);
                TempData["InfoMessage"] = "Se ha logrado registrar en el sistema satisfactoriamente";
                return RedirectToAction("Login", "User");
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
                return View("RegisterPerson",model);
            }
        }


        // GET: Person/Edit
        [Authorize(Roles = "Person")]
        public ActionResult Edit()
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"].ToString();
                TempData.Remove("ErrorMessage");
            }
            HttpCookie myCookie = Request.Cookies["UserData"];
            PersonDTO person = new PersonDTO { Id = int.Parse(myCookie["Id"]), Account = "0" };
            person = this._personAppService.Get(person);
            return View("Edit", person);
        }

        // POST: Person/Edit
        [HttpPost]
        [Authorize(Roles = "Person")]
        public virtual ActionResult Edit(PersonDTO model)
        {
            try
            {
                model.Ciudad = new CityDTO { Id = int.Parse(Request.Form["Ciudad"]) };
                model.Id = int.Parse(Request.Cookies["UserData"]["Id"]);

                int cantidadTelefonos = int.Parse(Request.Form["CantidadTelefonos"]);

                ///Controlo que al menos tenga un telefono
                if (cantidadTelefonos > 0)
                {
                    ///Obtengo la lista de los telefonos
                    List<PhoneDTO> pTelefonos = new List<PhoneDTO>();
                    for (int i = 0; i < cantidadTelefonos; i++)
                    {
                        int codArea = int.Parse(Request.Form["codArea" + i]);
                        int telefono = int.Parse(Request.Form["telefono" + i]);
                        PhoneDTO phone = new PhoneDTO { CodigoArea = codArea, Numero = telefono };
                        pTelefonos.Add(phone);
                    }
                    model.Telefonos = pTelefonos;
                }
                else
                {
                    throw new UserFriendlyException("Debe registrarse al menos un telefono");
                }

                UpdatePersonInput personInput = this._objectMapper.Map<UpdatePersonInput>(model);
                this._personAppService.UpdatePerson(personInput);

                //Actualizamos las cookies
                Response.Cookies.Clear();
                HttpCookie myCookie = new HttpCookie("UserData");
                myCookie["Id"] = model.Id.ToString();
                myCookie["User"] = "rol_person";
                myCookie["NombreCompleto"] = model.Nombre + " " + model.Apellido.Substring(0, 1) + ".";
                Response.Cookies.Add(myCookie);

                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Edit");
            }
        }

        // GET: Person/GetPerson
        [Authorize(Roles = "Person, Intermediary")]
        public ActionResult GetPerson(FormCollection collection)
        {
            PersonDTO pPerson = new PersonDTO { Account = Convert.ToString(collection["idToGetPerson"]) };
            PersonDTO model = this._personAppService.GetByAccount(pPerson);
            return View("ShowPerson", model);
        }

        #region Requests

        // GET: Person/RegisterRequest
        [Authorize(Roles = "Person")]
        public ActionResult RegisterRequest()
        {
            return View("RegisterRequest", new RegisterRequestInput());
        }

        // POST: Person/RegisterRequest
        [HttpPost]
        [Authorize(Roles = "Person")]
        public virtual ActionResult RegisterRequest(RegisterRequestInput model, HttpPostedFileBase fileUpload)
        {
            try
            {
                int totalDrugsRequest = int.Parse(Request.Form["totalDrugsRequest"]);
                ///Controlo que al menos tenga un medicamento
                if (totalDrugsRequest > 0)
                {
                    ///Obtengo la lista de los medicamentos
                    List<DrugsRequestDTO> drugsRequests = new List<DrugsRequestDTO>();
                    for (int i = 0; i < totalDrugsRequest; i++)
                    {
                        int quantityDrug = int.Parse(Request.Form[i + "_quantityDrug"]);
                        int idDrug = int.Parse(Request.Form[i + "_idDrug"]);
                        DrugsRequestDTO drugRequest = new DrugsRequestDTO() { Cantidad = quantityDrug, Medicamento = new DrugDTO() { Id = idDrug } };
                        drugsRequests.Add(drugRequest);
                    }
                    model.Medicamentos = drugsRequests;
                }
                else
                {
                    throw new UserFriendlyException("Debe seleccionar al menos un medicamento");
                }

                HttpCookie myCookie = Request.Cookies["UserData"];
                PersonDTO person = new PersonDTO { Id = int.Parse(myCookie["Id"]), Account = "0" };
                model.Persona = person;

                var extensionFile = Path.GetExtension(fileUpload.FileName);
                string[] allowExtension = new string[] { ".jpg", ".png", ".jpeg", ".pdf" };
                if(Array.IndexOf(allowExtension,extensionFile)<0)
                {
                    throw new UserFriendlyException("El formato de la receta no es válido");
                }

                Guid g = Guid.NewGuid();
                var fileName = Path.GetFileName(fileUpload.FileName);
                fileName = g + fileName;
                
                var path = Path.Combine(Server.MapPath("~/images/recetas/"), fileName);
                
                fileUpload.SaveAs(path);
                model.RutaReceta = fileName;

                this._personAppService.RegisterRequest(model);
                TempData["InfoMessage"] = "La solicitud fue registrada satisfactoriamente";
                return RedirectToAction("DashboardPerson");
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
                return RedirectToAction("RegisterRequest");
            }
        }

        // GET: Person/EditRequest/5
        [Authorize(Roles = "Person")]
        public ActionResult EditRequest(int id)
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"].ToString();
                TempData.Remove("ErrorMessage");
            }

            PersonDTO person = new PersonDTO { Id = Convert.ToInt32(Request.Cookies["UserData"]["Id"]) };
            GetRequestsOutput request = this._personAppService.GetRequest(new GetRequestsInput { Id = id,Persona=person });
            return View("EditRequest", request);
        }


        // POST: Person/EditRequest
        [HttpPost]
        [Authorize(Roles = "Person")]
        public virtual ActionResult EditRequest(GetRequestsOutput model, HttpPostedFileBase fileUpload)
        {
            try
            {

                int totalDrugsRequest = int.Parse(Request.Form["totalDrugsRequest"]);

                ///Controlo que al menos tenga un medicamento
                if (totalDrugsRequest > 0)
                {
                    ///Obtengo la lista de los medicamentos
                    List<DrugsRequestDTO> drugsRequests = new List<DrugsRequestDTO>();

                    for (int i = 0; i < totalDrugsRequest; i++)
                    {
                        int quantityDrug = int.Parse(Request.Form[i + "_quantityDrug"]);
                        int idDrug = int.Parse(Request.Form[i + "_idDrug"]);
                        DrugsRequestDTO drugRequest = new DrugsRequestDTO() { Cantidad = quantityDrug, Medicamento = new DrugDTO() { Id = idDrug } };
                        drugsRequests.Add(drugRequest);
                    }
                    model.Medicamentos = drugsRequests;
                }
                else
                {
                    throw new UserFriendlyException("Debe seleccionar al menos un medicamento");
                }

                HttpCookie myCookie = Request.Cookies["UserData"];
                PersonDTO person = new PersonDTO { Id = int.Parse(myCookie["Id"]), Account = "0" };
                model.Persona = person;

                if(fileUpload!=null)
                {
                    var extensionFile = Path.GetExtension(fileUpload.FileName);
                    string[] allowExtension = new string[] { ".jpg", ".png", ".jpeg", ".pdf" };
                    if (Array.IndexOf(allowExtension, extensionFile) < 0)
                    {
                        throw new UserFriendlyException("El formato de la receta no es válido");
                    }

                    Guid g = Guid.NewGuid();
                    var fileName = Path.GetFileName(fileUpload.FileName);
                    fileName = g + fileName;


                    var path = Path.Combine(Server.MapPath("~/images/recetas/"), fileName);

                    fileUpload.SaveAs(path);
                    model.RutaReceta = fileName;
                }              
                this._personAppService.UpdateRequest(this._objectMapper.Map<UpdateRequestInput>(model));
                return RedirectToAction("GetRequest",new { id = model.Id});

            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message;
                return RedirectToAction("EditRequest", new { id = model.Id });
            }

        }

        // GET: Person/GetRequest/5
        [Authorize(Roles = "Person")]
        public ActionResult GetRequest(int id)
        {
            try
            {
                PersonDTO pPerson = this._personAppService.Get(new PersonDTO { Id = int.Parse(Request.Cookies["UserData"]["Id"]), Account = "0" });
                GetRequestsInput request = new GetRequestsInput { Id = id, Persona = pPerson };
                GetRequestsOutput model = this._personAppService.GetRequest(request);
                return View("~/Views/Request/show.cshtml", model);
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message;
                return RedirectToAction("DashboardPerson");
            }
        }

        #endregion


        #region Donations

        // GET: Person/RegisterRequest
        [Authorize(Roles = "Person")]
        public ActionResult RegisterDonation()
        {
            return View("RegisterDonation", new RegisterDonationInput());
        }

        // POST: Person/RegisterDonation
        [HttpPost]
        [Authorize(Roles = "Person")]
        public ActionResult RegisterDonation(RegisterDonationInput model)
        {
            try
            {
                int totalDrugsDonation = int.Parse(Request.Form["totalDrugsDonation"]);

                ///Controlo que al menos tenga un medicamento
                if (totalDrugsDonation > 0)
                {
                    ///Obtengo la lista de los medicamentos
                    List<DrugsDonationDTO> drugsDonation = new List<DrugsDonationDTO>();

                    for (int i = 0; i < totalDrugsDonation; i++)
                    {
                        int quantityDrug = int.Parse(Request.Form[i + "_quantityDrug"]);
                        int idDrug = int.Parse(Request.Form[i + "_idDrug"]);

                        string[] expirationDateString = (Request.Form[i + "_expirationDateDrug"]).Split('-');
                        DateTime expirationDate = new DateTime(int.Parse(expirationDateString[1]), int.Parse(expirationDateString[0]), 1);                       
                        DrugsDonationDTO drugDonation = new DrugsDonationDTO() { Cantidad = quantityDrug, Medicamento = new DrugDTO() { Id = idDrug },FechaVencimiento=expirationDate };
                        drugsDonation.Add(drugDonation);
                    }
                    model.Medicamentos = drugsDonation;
                }
                else
                {
                    throw new UserFriendlyException("Debe seleccionar al menos un medicamento");
                }

                HttpCookie myCookie = Request.Cookies["UserData"];
                PersonDTO person = new PersonDTO { Id = int.Parse(myCookie["Id"]), Account = "0" };
                model.Persona = person;

                this._personAppService.RegisterDonation(model);
                TempData["InfoMessage"] = "La donacion fue registrada satisfactoriamente";
                return RedirectToAction("DashboardPerson");

            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
                return RedirectToAction("RegisterDonation");
            }
        }

        // GET: Person/EditDonation/5
        [Authorize(Roles = "Person")]
        public ActionResult EditDonation(int id)
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"].ToString();
                TempData.Remove("ErrorMessage");
            }
            PersonDTO person = new PersonDTO { Id = Convert.ToInt32(Request.Cookies["UserData"]["Id"])};
            GetDonationsOutput donation = this._personAppService.GetDonation(new GetDonationsInput { Id = id, Persona = person });
            return View("EditDonation", donation);
        }

        // POST: Person/EditDonation
        [HttpPost]
        [Authorize(Roles = "Person")]
        public ActionResult EditDonation(GetDonationsOutput model)
        {
            try
            {
                int totalDrugsDonation = int.Parse(Request.Form["totalDrugsDonation"]);

                ///Controlo que al menos tenga un medicamento
                if (totalDrugsDonation > 0)
                {
                    ///Obtengo la lista de los medicamentos
                    List<DrugsDonationDTO> drugsDonations = new List<DrugsDonationDTO>();

                    for (int i = 0; i < totalDrugsDonation; i++)
                    {
                        int quantityDrug = int.Parse(Request.Form[i + "_quantityDrug"]);
                        int idDrug = int.Parse(Request.Form[i + "_idDrug"]);

                        string[] expirationDateString = (Request.Form[i + "_expirationDateDrug"]).Split('-');
                        DateTime expirationDate = new DateTime(int.Parse(expirationDateString[1]), int.Parse(expirationDateString[0]), 1);
                        DrugsDonationDTO drugDonation = new DrugsDonationDTO() { Cantidad = quantityDrug, Medicamento = new DrugDTO() { Id = idDrug }, FechaVencimiento = expirationDate };
                        drugsDonations.Add(drugDonation);
                    }
                    model.Medicamentos = drugsDonations;
                }
                else
                {
                    throw new UserFriendlyException("Debe seleccionar al menos un medicamento");
                }

                this._personAppService.UpdateDonation(this._objectMapper.Map<UpdateDonationInput>(model));
                return RedirectToAction("GetDonation", new { id = model.Id });

            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message;
                return RedirectToAction("EditDonation",new { id = model.Id });
            }
        }

        // GET: Person/GetDonation
        [Authorize(Roles = "Person")]
        public ActionResult GetDonation(int id) {

            try
            {
                PersonDTO pPerson = this._personAppService.Get(new PersonDTO { Id = int.Parse(Request.Cookies["UserData"]["Id"]), Account = "0" });
                GetDonationsInput donation = new GetDonationsInput { Id = id, Persona = pPerson };
                GetDonationsOutput model = this._personAppService.GetDonation(donation);
                return View("~/Views/Donation/Show.cshtml", model);
            }
            catch(Exception e)
            {
                TempData["ErrorMessage"] = e.Message;
                return RedirectToAction("DashboardPerson");
            }
            
        }

        #endregion

        // GET: Person/DashboardPerson
        [Authorize(Roles = "Person")]
        public ActionResult DashboardPerson()
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"].ToString();
                TempData.Remove("ErrorMessage");
            }

            if (TempData["InfoMessage"] != null)
            {
                ViewBag.InfoMessage = TempData["InfoMessage"].ToString();
                TempData.Remove("InfoMessage");
            }
            HttpCookie myCookie = Request.Cookies["UserData"];
            var model = new ListPersonModel(_personAppService.GetAllRequests(), _personAppService.GetAllDonations());
            model.requests = model.requests.FindAll(x => x.Persona.Id.ToString() == myCookie["Id"]);
            model.donations = model.donations.FindAll(x => x.Persona.Id.ToString() == myCookie["Id"]);
            return View("DashboardPerson", model);
        }

        // GET: Person/CancelRequest/5
        [Authorize(Roles = "Person")]
        public virtual ActionResult CancelRequest(int id)
        {
            PersonDTO person = new PersonDTO { Id = int.Parse(Request.Cookies["UserData"]["Id"]), Account = "0" };
            this._personAppService.CancelRequest(new CancelRequestInput { Id = id, Persona = person });
            TempData["InfoMessage"] = "La solicitud fue cancelada satisfactoriamente";
            return RedirectToAction("Index");
        }

        // GET: Person/CancelDonation/5
        [Authorize(Roles = "Person")]
        public virtual ActionResult CancelDonation(int id)
        {
            PersonDTO person = new PersonDTO { Id = int.Parse(Request.Cookies["UserData"]["Id"]), Account = "0" };
            this._personAppService.CancelDonation(new CancelDonationInput { Id = id, Persona = person });
            TempData["InfoMessage"] = "La donacion fue cancelada satisfactoriamente";
            return RedirectToAction("Index");
        }

    }
}

