﻿

function UpdateListCheck()
{
    //$('.formCheck').submit();
}

function UpdateList() {
    var iSelect = selectChangeable;

    switch (iSelect) {
        case 1: $('select.display').last().val($('select.display').first().val());
            break;
        case 2: $('select.display').first().val($('select.display').last().val());
            break;
    }

    var val1 = $('select.display').first().val();
    var val2 = $('select.display').last().val();

    if ((val1 == "Solicitudes") && (val2 == "Solicitudes")) {
        $('#requestsTable').show();
        $('#donationsTable').hide();
        selectChangeable = 1;
    }
    else if ((val1 == "Donaciones") && (val2 == "Donaciones")) {
        $('#requestsTable').hide();
        $('#donationsTable').show();
        selectChangeable = 2;
    }
}

function openDialogRequestState(idRequest, stateActual) {
    console.log(stateActual);
    switch (stateActual) {
        case "EnDistribucion":
            $("#RequestState").val("2");
            break;
        case "Entregada":
            $("#RequestState").val("3");
            break;
        case "Cancelada":
            $("#RequestState").val("4");
            break;
        default: break;
    }
    $("#idRequest").val(idRequest);
    $("#dialogRequestState").dialog("open");
}

function openDialogDonationState(idDonation, stateActual) 
{
    console.log(stateActual);
    switch (stateActual) {
        case "EnDistribucion":
            $("#RequestState").val("2");
            break;
        case "Entregada":
            $("#RequestState").val("3");
            break;
        case "Cancelada":
            $("#RequestState").val("4");
            break;
        default: break;
    }
    $("#idDonation").val(idDonation);
    $("#dialogDonationState").dialog("open");
}