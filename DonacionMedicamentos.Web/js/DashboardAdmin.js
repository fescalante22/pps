﻿
function InitialTable() {
    $('table.display').DataTable({
        select: true,
        lenghtChange: false,
        lengthMenu: [10],
        language:
        {
            "sSearch": "Buscar:",
            "oPaginate":
                {
                    "sFirst": "Primero",
                    "sPrevious": "Anterior",
                    "sNext": "Siguiente",
                    "sLast": "Último"
                },
            "lengthMenu": "Mostrar los primeros _MENU_ registros",
            "zeroRecords": "No se han encontrado resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No se encontraron registros",
            "infoFiltered": "(filtrado de _MAX_ registros en total)"
        },
        dom: 'rf <"panel" <"toolbar">> <"tabla" t> ip'
    });
    $("div.toolbar").html(
        '<a href="/Admin/CreateIntermediary"><button class="btn btn-primary btn-sm">Registrar Intermediario</button></a>'
        );
}

$(function () {
    $("#divToTrashIntermediary").dialog({
        autoOpen: false,
        height: 200,
        width: 400,
        dialogClass: "ui-dialog",
        modal: true,
        buttons: {
            "Eliminar": function () {
                $('#formTrashIntermediary').submit();
            },
            "Cancelar": function () {
                $(this).dialog('close');
            }
        },
        close: function () {
            $("#formTrashIntermediary")[0].reset();
        }
    });
});

$.fn.button.noConflict();

function trashIntermediary(pId) {
    $("#idToDelete").val(pId);
    $("#divToTrashIntermediary").dialog("open");
}