﻿function InitialTable()
{
    $('table.display').DataTable({
        select: true,
        lenghtChange: false,
        lengthMenu: [10],
        language:
        {
            "sSearch": "Buscar:",
            "oPaginate":
                {
                    "sFirst": "Primero",
                    "sPrevious": "Anterior",
                    "sNext": "Siguiente",
                    "sLast": "Último"
                },
            "lengthMenu": "Mostrar los primeros _MENU_ registros",
            "zeroRecords": "No se han encontrado resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No se encontraron registros",
            "infoFiltered": "(filtrado de _MAX_ registros en total)"
        },
        dom: 'rf <"panel" <"toolbar">> <"tabla" t> ip'
    });
    $("div.toolbar").html(
        '<a href="/Person/RegisterRequest"><button class="btn btn-primary btn-sm">Agregar Solicitud</button></a>'+
        '<a href="/Person/RegisterDonation"><button class="btn btn-primary btn-sm">Agregar Donacion</button></a>'+
        '<div style="float:right; margin-right:10px">' +
            '<select id="OptionList" class="display" onchange="UpdateList()">'+
                '<option value="Solicitudes">Solicitudes</option>'+
                '<option value="Donaciones">Donaciones</option>'+
            '</select>'+
        '</div>'    
        );
}

function UpdateList()
{
    var iSelect = selectChangeable;
    switch (iSelect) {
        case 1: $('select.display').last().val($('select.display').first().val());
            break;
        case 2: $('select.display').first().val($('select.display').last().val());
            break;
    }
    var val1 = $('select.display').first().val();
    var val2 = $('select.display').last().val();
    if ((val1 == "Solicitudes") && (val2 == "Solicitudes")) {
        $('#requestsTable').show();
        $('#donationsTable').hide();
        selectChangeable = 1;
    }
    else if ((val1 == "Donaciones") && (val2 == "Donaciones")) {
        $('#requestsTable').hide();
        $('#donationsTable').show();
        selectChangeable = 2;
    }
}



