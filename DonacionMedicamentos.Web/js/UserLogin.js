﻿
var radioInput = ($('input:radio[name=Rol]'));
var loginType = '';
function changeInputs()
{
    if ($('#radio_person').is(':checked'))
    {
        loginType = 'person';
        $("#DNI").rules("add", {
            required: true
        });
        $("#CUIT").rules("add", {
            required: false
        });
        $('#formPerson').show(500);
        $('#formIntermediary').hide(500);
    }
    else {
        loginType = 'intermediary';
        $("#DNI").rules("add", {
            required: false
        });
        $("#CUIT").rules("add", {
            required: true
        });
        $('#formPerson').hide(500);
        $('#formIntermediary').show(500);
    }
}

$(document).ready(function () {
    $("#formLogin").validate({
        errorClass: "validate-error-class",
        messages: {
            CUIT: "Por favor ingrese un CUIT/CUIL",
            DNI: "Por favor ingrese un DNI",
            Contraseña: "Por favor ingrese una contraseña",

        }
    });
    changeInputs();
    $('#CUIT').mask('00-00000000-0');
    $('#DNI').mask('00000000');
});
        
radioInput.click(function () {
    changeInputs();
});

function login()
{
    if(loginType=='person')
    {
        $('#Account').val($('#DNI').cleanVal());
    }
    else
    {
        $('#Account').val($('#CUIT').cleanVal());
    }
    $('#formLogin').submit();
}

function recoverPass() {
    if (loginType == 'person') {
        $('#Account').val($('#DNI').cleanVal());
    }
    else {
        $('#Account').val($('#CUIT').cleanVal());
    }
    $('#formRecoverPass').submit();
}