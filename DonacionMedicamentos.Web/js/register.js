﻿function obtenerProvincias() {
    var select = $('#Provincia');
    $.ajax({
        url: '/Location/States',
        type: 'GET',
        dataType: 'json',
        success: function (json) {          
            select.empty();
            $.each(json, function (i, val) {
                select.append($('<option />', { value: val.Id, text: val.Nombre }));
            });
            if (idProvincia != null) { select.val(idProvincia) };
            obtenerCiudades();
        }
    });
}


function inicializarIdsCiudades()
{
    idProvincia = null;
    idCiudad = null;
}

function obtenerCiudades() {
    data = { idState: $("#Provincia option:selected").val() };
    var select = $('#Ciudad');
    $.ajax({
        url: '/Location/Cities',
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function (json) {
            select.empty();
            $.each(json, function (i, val) {
                select.append($('<option />', { value: val.Id, text: val.Nombre }));
                if (idCiudad != null) { select.val(idCiudad) } else {
                    select.val($("#Ciudad option:first").val());
                };
            });
        }
    });
    
}

var cantidadTelefonos = 0;
var cantidadMaximaTelefonos = 3;


$(document).ready(function () {
    $("#btnAdd").bind("click", function () {
        if ($("#CantidadTelefonos").val() < cantidadMaximaTelefonos)
        {
            agregarCampo();
        }
        else
        {
            $("#limit-telephones-alert").fadeTo("slow", 1);
            setTimeout(function()
            {
                $("#limit-telephones-alert").slideUp(3000);
            }, 3000);

        }         
    });
    $("#btnGet").bind("click", function () {
        obtenerCampos();
    });
    $("body").on("click", ".remove", function () {
        $(this).closest("div").remove();
        obtenerCamposTelefonos();
        actualizarCampoCantidadTelefonos();
    });

    obtenerCamposTelefonos();
    actualizarCampoCantidadTelefonos();
    obtenerProvincias();
});

function actualizarCampoCantidadTelefonos()
{
    var i = 0;
    $("input[id=dynamicCodArea]").each(function () {
        i++;
    });
    cantidadTelefonos = i;
    $("#CantidadTelefonos").val(cantidadTelefonos);
    console.log(cantidadTelefonos);
    console.log($("#CantidadTelefonos").val())
}

function agregarCampo(obligatorio)
{
    var div = $("<div />");
    div.html(GetDynamicTextBox("", obligatorio));
    $("#TextBoxContainer").append(div);
    obtenerCamposTelefonos();
    actualizarCampoCantidadTelefonos();
}

function obtenerCamposTelefonos()
{
    console.log("Entró a la función de obtener campos");
    var values = "";
    var i = 0;
    $("input[id=dynamicCodArea]").each(function () {
        $(this).attr('name', 'codArea' + i);
        i++;
    });

    i = 0;
    $("input[id=dynamicTelefono]").each(function () {
        $(this).attr('name', 'telefono' + i);
        i++;
    });
}


function GetDynamicTextBox(value, obligatorio) {
    var campoBorrar = '';
    if (!obligatorio)
    {
        campoBorrar = '<a style="margin-top:10px;color:red; cursor:pointer;" class="remove glyphicon glyphicon-minus"></a>';
    }
    return '<div class="row camposTelefonos"><div class="col-md-3"><input id="dynamicCodArea" class="form-control codArea" name="" type="text"  value="" required></input></div> <div class="col-md-1"><p>-</p></div> <div class="col-md-7"><input class="form-control telefono" id="dynamicTelefono" name="" type="text" value="" required /></div>' + campoBorrar;
     
}

function inicializarCampos() {
    $('#FechaNacimiento').val('');
    $('#Piso').val('');
    $('#Departamento').val('');
    $('#Numero').val('');
    $('#Email').val('');
    $('#Password').val('');
}


function GetAccountUnMask()
{
    $('#Account').val($('#CUIT').cleanVal());
}