﻿//-------------------------------------------------------------------
//
//            ---        REGION REQUESTS AND DONATIONS       ---
//
//-------------------------------------------------------------------

$(document).ready(function () {
    $(".inputAddDrugs").mask("0000");

    dialogInvalidInput = $("#dialog-message-invalidInput").dialog({
        modal: true,
        autoOpen: false,
        buttons: {
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

});
var drugsArray = [];

//-------------------------------------------------------------------
//
//            ---        REGION REQUESTS        ---
//
//-------------------------------------------------------------------



//Inicializa la tabla de solicitudes
function InitializeTableRequest()
{
    $('#tableDrugsRequest').DataTable({
        "ajax": "/Drug/GetAll",
        rowId: 'Id',
        "columns": [
         { "data": "NombreGenerico" },
         { "data": "NombreComercial" },
         { "data": "Dosis"},
         {
             "data": "Id",
             "render": function (data, type, full, meta) {
                 return '<input type="text" size="4" class="inputAddDrugs" id="inputCountDrugs_' + data + '"/>'
             }
         },
         {
             "data": "Id",
             "render": function (data, type, full, meta) {

                 return '<input type="button" onClick="loadDrugRequest(' + data + ')" class="btn btn-success btn-xs" value="Añadir">';
             }
         }
        ],
        compact: true,
        ordering: false,
        dom: 'rf <"tabla" t> ip',
        lenghtChange: false,
        lengthMenu: [4],
        language:
                {
                    "sSearch": "Buscar:",
                    "oPaginate":
                        {
                            "sFirst": "Primero",
                            "sPrevious": "Anterior",
                            "sNext": "Siguiente",
                            "sLast": "Último"
                        },
                    "lengthMenu": "Mostrar los primeros _MENU_ registros",
                    "zeroRecords": "",
                    "loadingRecords": "<hr />Cargando los medicamentos. Espere por favor... <hr />",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No se encontraron registros",
                    "infoFiltered": "(filtrado de _MAX_ registros en total)"
                }
    });
}



// Carga un medicamento en la lista de solicitudes
function loadDrugRequest(idRow) {
    var table = $('#tableDrugsRequest').DataTable();
    var row = table.row("#" + idRow)
    var data = row.data();
    var cantidad = $("#inputCountDrugs_" + idRow).val();
    console.log(cantidad);
    if (cantidad != '')
    {
        var Drug = drugsArray.find(x=>x.Id == data['Id']);
        if (Drug != null) {
            Drug.Cantidad += parseInt(cantidad);
        }
        else {
            Drug = { Id: data['Id'], NombreGenerico: data['NombreGenerico'], NombreComercial: data['NombreComercial'], Dosis: data['Dosis'], Cantidad: parseInt(cantidad) };
            drugsArray.push(Drug);
        }

        completeRequest();
    }
    else {
        dialogInvalidInput.dialog("open");
    }
}



// Completa la lista de medicamentos solicitados
function completeRequest() {
    $("#listDrugsRequest").html("");
    $.each(drugsArray, function (key, value) {
        $("#listDrugsRequest").append('<div class="list-group-item">'
            + '<div class="list-group-item-heading"><div class="col-md-10"><p>' + value['NombreComercial'] + ' (' + value['NombreGenerico'] + ') - ' + value['Dosis'] + ' - ' + value['Cantidad'] + ' unidades</p></div>'
            + '<div class="text-right"><input type="button" class="btn btn-danger btn-sm" onClick="deleteDrugRequest(' + key + ')" value="Eliminar"></div></div>'
            + '<input name="' + key + '_idDrug" type="hidden" value="' + value['Id'] + '">'
            + '<input name="' + key + '_quantityDrug" type="hidden" value="' + value['Cantidad'] + '">'
            + '</div>');
    });

    $("#totalDrugsRequest").val(drugsArray.length);
    console.log(drugsArray);

}



// Elimina un medicamento solicitado
function deleteDrugRequest(drugRequest) {
    drugsArray.splice(drugRequest,1);
    completeRequest();
}



//Valida los archivos de la receta
function validFile() {
    var stringReceta = $("#recetaFile").val();
    var extension = stringReceta.substr(stringReceta.length - 3);
    extension = extension.toLowerCase();
    var allowExtension = ["jpg", "png", "jpeg", "pdf"];
    if ($.inArray(extension, allowExtension) < 0) {
        $("#alertInvalidFormat").show(1000);
    }
    else {
        $("#alertInvalidFormat").hide(1000);

    }
}





//-------------------------------------------------------------------
//
//            ---        REGION DONATIONS        ---
//
//-------------------------------------------------------------------



function InitializeTableDonation() {
    $('#tableDrugsDonation').DataTable({
        "ajax": 
            {
                "url": "/Drug/GetAll"
            },
        rowId: 'Id',
        "columns": [
         { "data": "NombreGenerico" },
         { "data": "NombreComercial" },
         { "data": "Dosis" },
         {
             "data": "Id",
             "render": function (data, type, full, meta) {
                 return '<input type="text" size="4" class="inputAddDrugs" id="inputQuantityDrugs_' + data + '"/>'
             }
         },
         {
             "data": "Id",
             "render": function (data, type, full, meta) {
                 return '<select class="inputMonth" id="inputMonthExpirationDateDrugs_' + data + '"/></select>-<select class="inputYear" id="inputYearExpirationDateDrugs_' + data + '"/></select>'
             }
         },
         {
             "data": "Id",
             "render": function (data, type, full, meta) {

                 return '<input type="button" onClick="loadDrugDonation(' + data + ')" class="btn btn-success btn-xs" value="Añadir">';
             }
         }
        ],
        compact: true,
        ordering: false,
        dom: 'rf <"tabla" t> ip',
        lenghtChange: false,
        lengthMenu: [4],
        language:
                {
                    "sSearch": "Buscar:",
                    "oPaginate":
                        {
                            "sFirst": "Primero",
                            "sPrevious": "Anterior",
                            "sNext": "Siguiente",
                            "sLast": "Último"
                        },
                    "lengthMenu": "Mostrar los primeros _MENU_ registros",
                    "zeroRecords": "",
                    "loadingRecords": "<hr />Cargando los medicamentos. Espere por favor... <hr />",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No se encontraron registros",
                    "infoFiltered": "(filtrado de _MAX_ registros en total)"
                }
    });
}

var optionsMonth='';
var optionsYear='';

///Inicializa los meses y años en el select
function initializeMonthAndYear()
{
    //Inicializo los meses
    for(var i=1;i<=12;i++)
    {
        optionsMonth+=('<option>' + ("0" + i).slice(-2) + '</option>');
    }

    //Inicializo los años
    for (var j = 2017; j <= 2050; j++) {
        optionsYear += ('<option>' + ("0" + j).slice(-4) + '</option>')
    }
}

function updateMonthAndYear()
{
    $(".inputMonth").append(optionsMonth);
    $(".inputYear").append(optionsYear);
}


$('#tableDrugsDonation').on('draw.dt', function () {
    updateMonthAndYear();
 });

// Carga un medicamento en la lista de donaciones
function loadDrugDonation(idRow) {
    var table = $('#tableDrugsDonation').DataTable();
    var row = table.row("#" + idRow)
    var data = row.data();
    var quantity = $("#inputQuantityDrugs_" + idRow).val();
    var expirationDate = $("#inputMonthExpirationDateDrugs_" + idRow).val() + '-' + $("#inputYearExpirationDateDrugs_" + idRow).val();

    console.log(quantity);
    if (quantity != '')
    {
        var Drug = drugsArray.find(x=>x.Id == data['Id'] && x.ExpirationDate == expirationDate);
        if (Drug != null) {
            Drug.Quantity += parseInt(quantity);
        }
        else {
            Drug = { Id: data['Id'], NombreGenerico: data['NombreGenerico'], NombreComercial: data['NombreComercial'], Dosis: data['Dosis'], Quantity: parseInt(quantity), ExpirationDate: expirationDate };
            drugsArray.push(Drug);
        }

        completeDonation();
    }
    else
    {
        dialogInvalidInput.dialog("open");
    }
}



// Completa la lista de medicamentos donados
function completeDonation() {
    $("#listDrugsDonation").html("");
    $.each(drugsArray, function (key, value) {
        $("#listDrugsDonation").append('<div class="list-group-item">'
            + '<div class="list-group-item-heading"><div class="col-md-10"><p>' + value['NombreComercial'] + ' (' + value['NombreGenerico'] + ') | ' + value['Dosis'] + ' | ' + value['Quantity'] + ' unidades' + ' | Vto. ' + value['ExpirationDate'] + '</p></div>'
            + '<div class="text-right"><input type="button" class="btn btn-danger btn-sm" onClick="deleteDrugDonation(' + key + ')" value="Eliminar"></div></div>'
            + '<input name="' + key + '_idDrug" type="hidden" value="' + value['Id'] + '">'
            + '<input name="' + key + '_quantityDrug" type="hidden" value="' + value['Quantity'] + '">'
            + '<input name="' + key + '_expirationDateDrug" type="hidden" value="' + value['ExpirationDate'] + '">'
            + '</div>');
    });
    $("#totalDrugsDonation").val(drugsArray.length);
}




// Elimina un medicamento en donación
function deleteDrugDonation(drugDonation) {
    drugsArray.splice(drugDonation,1);
    completeDonation();
}

