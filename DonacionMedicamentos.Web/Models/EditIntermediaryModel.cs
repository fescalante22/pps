﻿using DonacionMedicamentos.Intermediarios.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DonacionMedicamentos.Web.Models
{
    public class EditIntermediaryModel
    {
        public virtual UpdateIntermediaryInput input { get; set; }
        public virtual IntermediaryDTO output { get; set; }

        public EditIntermediaryModel(
           UpdateIntermediaryInput pInput,
           IntermediaryDTO pOutput)
        {
            input = pInput;
            output = pOutput;
        }
    }
}