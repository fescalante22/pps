﻿using DonacionMedicamentos.Donations.DTO;
using DonacionMedicamentos.Intermediarios.DTO;
using DonacionMedicamentos.Requests.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DonacionMedicamentos.Web.Models
{
    public class ListIntermediaryModel
    {
        public virtual List<GetRequestsOutput> requests { get; set; }
        public virtual List<GetDonationsOutput> donations { get; set; }

        public ListIntermediaryModel(
            List<GetRequestsOutput> pRequests,
            List<GetDonationsOutput> pDonations)
        {
            requests = pRequests;
            donations = pDonations;
        }

    }
}