﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DonacionMedicamentos.Web.Models
{
    public class CityAndStateModel
    {

        public IEnumerable<SelectListItem> States { get; set; }


        public IEnumerable<SelectListItem> Cities { get; set; }
    }
}