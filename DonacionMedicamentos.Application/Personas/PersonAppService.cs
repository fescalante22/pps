﻿using System.Collections.Generic;
using System.Linq;
using DonacionMedicamentos.Personas.DTO;
using DonacionMedicamentos.Phones;
using DonacionMedicamentos.Cities;
using DonacionMedicamentos.Donations;
using DonacionMedicamentos.Requests;
using DonacionMedicamentos.DrugsDonations;
using Abp.ObjectMapping;
using DonacionMedicamentos.DrugsRequests;
using DonacionMedicamentos.Drugs;
using System;
using DonacionMedicamentos.Intermediarios.DTO;
using DonacionMedicamentos.Requests.DTO;
using DonacionMedicamentos.Donations.DTO;
using DonacionMedicamentos.Locations.DTO;
using DonacionMedicamentos.RequestStates;
using DonacionMedicamentos.DonationStates;

namespace DonacionMedicamentos.Personas
{
    /// <summary>
    /// Servicio de Aplicacion de Persona
    /// </summary>
    public class PersonAppService : IPersonAppService
    {
        //Instanciamos IObjectMapper para realizar el mapeo de los objetos
        private readonly IObjectMapper _objectMapper;
        //Instanciamos los managers necesarios para delegar la logica
        private readonly IPersonManager _personManager;
        private readonly IRequestManager _requestManager;
        private readonly IDonationManager _donationManager;
        private readonly ICityManager _cityManager;
        private readonly DrugManager _drugManager;



        /// <summary>
        /// Constructor del servicio de aplicacion de Persona
        /// </summary>
        public PersonAppService(
            PersonManager personManager, 
            RequestManager requestManager, 
            DonationManager donationManager, 
            IObjectMapper objectMapper,
            CityManager cityManager,
            DrugManager drugManager)
        {
            _personManager = personManager;
            _requestManager = requestManager;
            _donationManager = donationManager;
            _objectMapper = objectMapper;
            this._cityManager = cityManager;
            _drugManager = drugManager;
        }

        /// <summary>
        /// Metodo para cancelar una donacion de la persona
        /// </summary>
        /// <param name="input"></param>
        public void CancelDonation(CancelDonationInput input)
        {
            Donation donacion = this._donationManager.Get(this._objectMapper.Map<Donation>(input));
            donacion.Estado = DonationState.Cancelada;
            this._donationManager.ChangeDonationState(donacion);
        }

        /// <summary>
        /// Metodo para cancelar una solicitud de la persona
        /// </summary>
        /// <param name="input"></param>
        public void CancelRequest(CancelRequestInput input)
        {
            Request solicitud = this._requestManager.Get(this._objectMapper.Map<Request>(input));
            solicitud.Estado = RequestState.Cancelada;
            this._requestManager.ChangeRequestState(solicitud);
        }

        /// <summary>
        /// Metodo para crear una persona
        /// </summary>
        /// <param name="input"></param>
        public void CreatePerson(CreatePersonInput input)
        {
            City city = this._cityManager.Get(this._objectMapper.Map<City>(input.Ciudad));
            Person person = this._objectMapper.Map<Person>(input);
            person.Ciudad = city;
            this._personManager.Create(person);
        }

        /// <summary>
        /// Metodo para registrarle una donacion a la persona
        /// </summary>
        /// <param name="input"></param>
        public void RegisterDonation(RegisterDonationInput input)
        {
            List<DrugsDonation> listDrugsDonation = new List<DrugsDonation>();
            foreach (var item in input.Medicamentos)
            {
                Drug drug = this._drugManager.Get(_objectMapper.Map<Drug>(item.Medicamento));
                DrugsDonation drugDonation = new DrugsDonation() { Cantidad = item.Cantidad, Medicamento = drug,FechaVencimiento = item.FechaVencimiento };
                listDrugsDonation.Add(drugDonation);
            }

            Donation donation = this._objectMapper.Map<Donation>(input);
            donation.Medicamentos = listDrugsDonation;
            donation.Persona = this._personManager.Get(this._objectMapper.Map<Person>(input.Persona));

            this._donationManager.RegisterDonation(donation);
        }

        /// <summary>
        /// Método que obtiene una persona
        /// </summary>
        /// <param name="pAccount"></param>
        /// <returns></returns>
        public PersonDTO Get(PersonDTO pPerson)
        {
            Person person = this._objectMapper.Map<Person>(pPerson);
            return this._objectMapper.Map<PersonDTO>(_personManager.Get(person));
        }

        /// <summary>
        /// Método que obtiene una persona por el número de cuenta
        /// </summary>
        /// <param name="pAccount"></param>
        /// <returns></returns>
        public PersonDTO GetByAccount(PersonDTO pPerson)
        {
            Person person = this._objectMapper.Map<Person>(pPerson);
            return this._objectMapper.Map<PersonDTO>(_personManager.GetByAccount(person));
        }

        /// <summary>
        /// Metodo para registrarle una solicitud a la persona
        /// </summary>
        /// <param name="input"></param>
        public void RegisterRequest(RegisterRequestInput input)
        {              
            List<DrugsRequest> listDrugsRequests = new List<DrugsRequest>();
            foreach (var item in input.Medicamentos)
            {
                Drug drug = this._drugManager.Get(_objectMapper.Map<Drug>(item.Medicamento));
                DrugsRequest drugRequest = new DrugsRequest() { Cantidad = item.Cantidad, Medicamento = drug };
                listDrugsRequests.Add(drugRequest);
            }

            Request request = this._objectMapper.Map<Request>(input);
            request.Medicamentos = listDrugsRequests;
            request.Persona = this._personManager.Get(this._objectMapper.Map<Person>(input.Persona));          

            this._requestManager.RegisterRequest(request);
        }


        /// <summary>
        /// Metodo para modificar una donacion de la persona 
        /// </summary>
        /// <param name="input"></param>
        public void UpdateDonation(UpdateDonationInput input)
        {
            List<DrugsDonation> listDrugDonation = new List<DrugsDonation>();
            foreach (var item in input.Medicamentos)
            {
                Drug drug = this._drugManager.Get(_objectMapper.Map<Drug>(item.Medicamento));
                DrugsDonation drugDonation = new DrugsDonation() { Cantidad = item.Cantidad, Medicamento = drug,FechaVencimiento=item.FechaVencimiento };
                listDrugDonation.Add(drugDonation);
            }

            Donation donation = this._donationManager.Get(new Donation { Id = input.Id });
            this._objectMapper.Map(input, donation);
            donation.Medicamentos = listDrugDonation;
            this._donationManager.UpdateDonation(donation);
        }

        /// <summary>
        /// Metodo para modificar la persona 
        /// </summary>
        /// <param name="input"></param>
        public void UpdatePerson(UpdatePersonInput input)
        {
            input.Ciudad = this._objectMapper.Map<CityDTO>(this._cityManager.Get(this._objectMapper.Map<City>(input.Ciudad)));
            Person person = this._personManager.Get(this._objectMapper.Map<Person>(input));
            person.Ciudad = (this._cityManager.Get(this._objectMapper.Map<City>(input.Ciudad)));
            person = _objectMapper.Map(input, person);
            try
            {
                this._personManager.Update(person);
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
            
        }

        /// <summary>
        /// Metodo para modificar una solicitud de la persona 
        /// </summary>
        /// <param name="input"></param>
        public void UpdateRequest(UpdateRequestInput input)
        {
            List<DrugsRequest> listDrugsRequests = new List<DrugsRequest>();
            foreach (var item in input.Medicamentos)
            {
                Drug drug = this._drugManager.Get(_objectMapper.Map<Drug>(item.Medicamento));
                DrugsRequest drugRequest = new DrugsRequest() { Cantidad = item.Cantidad, Medicamento = drug };
                listDrugsRequests.Add(drugRequest);
            }

            Request request = this._requestManager.Get(new Request { Id = input.Id });
            this._objectMapper.Map(input, request);
            request.Medicamentos = listDrugsRequests;
            this._requestManager.UpdateRequest(request);
        }

        /// <summary>
        /// Obtiene una solicitud.
        /// </summary>
        /// <param name="pRequest"></param>
        /// <returns></returns>
        public GetRequestsOutput GetRequest(GetRequestsInput input)
        {
            Person person = this._personManager.Get(this._objectMapper.Map<Person>(input.Persona));
            return this._objectMapper.Map<GetRequestsOutput>(this._requestManager.Get(this._objectMapper.Map<Request>(input), person));
        }

        /// <summary>
        /// Obtiene todas las solicitudes
        /// </summary>
        /// <returns></returns>
        public List<GetRequestsOutput> GetAllRequests()
        {
            return this._objectMapper.Map<List<GetRequestsOutput>>(this._requestManager.GetRequests());
        }

        /// <summary>
        /// Obtiene todas las donaciones
        /// </summary>
        /// <returns></returns>
        public List<GetDonationsOutput> GetAllDonations()
        {
            return this._objectMapper.Map<List<GetDonationsOutput>>(this._donationManager.GetDonations());
        }

        /// <summary>
        /// Obtiene una donación
        /// </summary>
        /// <param name="pDonation"></param>
        /// <returns></returns>
        public GetDonationsOutput GetDonation(GetDonationsInput input)
        {
            Person person = this._personManager.Get(this._objectMapper.Map<Person>(input.Persona));
            return this._objectMapper.Map<GetDonationsOutput>(this._donationManager.Get(this._objectMapper.Map<Donation>(input),person));
        }
    }
}
