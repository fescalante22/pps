﻿using Abp.Application.Services;
using DonacionMedicamentos.Donations.DTO;
using DonacionMedicamentos.Intermediarios.DTO;
using DonacionMedicamentos.Personas.DTO;
using DonacionMedicamentos.Requests.DTO;
using System.Collections.Generic;

namespace DonacionMedicamentos.Personas
{
    public interface IPersonAppService : IApplicationService
    {
        void CreatePerson(CreatePersonInput input);

        void UpdatePerson(UpdatePersonInput input);

        void RegisterRequest(RegisterRequestInput input);
     
        void UpdateRequest(UpdateRequestInput input);

        void CancelRequest(CancelRequestInput input);

        void RegisterDonation(RegisterDonationInput input);

        void UpdateDonation(UpdateDonationInput input);

        void CancelDonation(CancelDonationInput input);

        PersonDTO GetByAccount(PersonDTO pPerson);

        List<GetRequestsOutput> GetAllRequests();

        List<GetDonationsOutput> GetAllDonations();

        GetRequestsOutput GetRequest(GetRequestsInput input);

        GetDonationsOutput GetDonation(GetDonationsInput input);
    }
}
