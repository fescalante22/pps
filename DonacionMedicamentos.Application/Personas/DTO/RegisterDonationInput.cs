﻿using Abp.AutoMapper;
using DonacionMedicamentos.Donations;
using DonacionMedicamentos.DrugsDonations;
using DonacionMedicamentos.Drugs.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Personas.DTO
{
    [AutoMapTo(typeof(Donation))]
    public class RegisterDonationInput
    {
        [Required]
        public virtual List<DrugsDonationDTO> Medicamentos { get; set; }

        [Required]
        public virtual PersonDTO Persona { get; set; }
    }
}
