﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DonacionMedicamentos.Drugs.DTO;
using DonacionMedicamentos.Requests;
using DonacionMedicamentos.Requests.DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DonacionMedicamentos.Personas.DTO
{
    [AutoMap(typeof(Request),typeof(GetRequestsOutput))]
    public class UpdateRequestInput : EntityDto
    {
        [Required]
        public virtual List<DrugsRequestDTO> Medicamentos { get; set; }

        [Required]
        public virtual string RutaReceta { get; set; }
    }
}
