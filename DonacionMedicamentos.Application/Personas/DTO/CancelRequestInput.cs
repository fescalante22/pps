﻿using Abp.AutoMapper;
using DonacionMedicamentos.Requests;
using System.ComponentModel.DataAnnotations;

namespace DonacionMedicamentos.Personas.DTO
{
    [AutoMapTo(typeof(Request))]
    public class CancelRequestInput
    {
        [Required]
        public virtual int Id { get; set; }

        [Required]
        public virtual PersonDTO Persona { get; set; }
    }
}