﻿using Abp.AutoMapper;
using DonacionMedicamentos.Drugs.DTO;
using DonacionMedicamentos.Requests;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DonacionMedicamentos.Personas.DTO
{
    [AutoMapTo(typeof(Request))]
    public class RegisterRequestInput
    {
        [Required]
        public List<DrugsRequestDTO> Medicamentos { get; set; }

        [Required]
        public string RutaReceta { get; set; }

        [Required]
        public virtual PersonDTO Persona { get; set; }
    }
}
