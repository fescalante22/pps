﻿using Abp.AutoMapper;
using DonacionMedicamentos.Cities;
using DonacionMedicamentos.Locations.DTO;
using DonacionMedicamentos.Phones;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Personas.DTO
{
    [AutoMapTo(typeof(Person))]
    public class CreatePersonInput
    {
        [Required]
        public virtual string Account { get; set; }

        public virtual string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public virtual string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public virtual string RepeatPassword { get; set; }

        [Required]
        public virtual string Nombre { get; set; }

        [Required]
        public virtual string Apellido { get; set; }

        public virtual string Genero { get; set; }

        [Required]
        public virtual string Calle { get; set; }

        [Required]
        public virtual int Numero { get; set; }

        public virtual int Piso { get; set; }

        public virtual int Departamento { get; set; }

        [Required]
        public virtual CityDTO Ciudad { get; set; }

        public virtual List<PhoneDTO> Telefonos { get; set; }

        public virtual DateTime FechaNacimiento { get; set; }

        public CreatePersonInput()
        {
            this.Telefonos = new List<PhoneDTO>();
        }
    }
}
