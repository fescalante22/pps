﻿using Abp.AutoMapper;
using DonacionMedicamentos.Donations;
using DonacionMedicamentos.Donations.DTO;
using DonacionMedicamentos.Drugs.DTO;
using DonacionMedicamentos.DrugsDonations;
using DonacionMedicamentos.Intermediarios.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Personas.DTO
{
    [AutoMap(typeof(Donation),typeof(GetDonationsOutput))]
    public class UpdateDonationInput
    {
        [Required]
        public virtual int Id { get; set; }

        [Required]
        public virtual List<DrugsDonationDTO> Medicamentos { get; set; }       
    }
}
