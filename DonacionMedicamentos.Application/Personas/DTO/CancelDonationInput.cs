﻿using Abp.AutoMapper;
using DonacionMedicamentos.Donations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Personas.DTO
{
    [AutoMapTo(typeof(Donation))]
    public class CancelDonationInput
    {
        [Required]
        public virtual int Id { get; set; }

        [Required]
        public virtual PersonDTO Persona { get; set; }
    }
}
