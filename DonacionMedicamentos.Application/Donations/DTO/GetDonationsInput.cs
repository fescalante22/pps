﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DonacionMedicamentos.Donations;
using DonacionMedicamentos.Drugs.DTO;
using DonacionMedicamentos.Intermediarios.DTO;
using DonacionMedicamentos.Personas.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DonacionMedicamentos.Donations.DTO
{
    [AutoMap(typeof(Donation))]
    public class GetDonationsInput : EntityDto
    {
        /// <summary>
        /// Propiedad para guardar la persona que registro la donacion
        /// </summary>
        public virtual PersonDTO Persona { get; set; }

        /// <summary>
        /// Propiedad para guardar el intermediario que aprobo la donacion
        /// </summary>
        public virtual IntermediaryDTO Intermediario { get; set; }
    }
}
