﻿using Abp.AutoMapper;
using System;
using System.ComponentModel.DataAnnotations;

namespace DonacionMedicamentos.Comments
{
    [AutoMap(typeof(Comment))]
    public class CommentDTO
    {
        [Required]
        public virtual string Detalle { get; set; }

        public virtual DateTime Fecha { get; set; }
    }
}
