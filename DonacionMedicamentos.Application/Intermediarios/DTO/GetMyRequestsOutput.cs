﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DonacionMedicamentos.Comments;
using DonacionMedicamentos.Drugs.DTO;
using DonacionMedicamentos.Personas.DTO;
using DonacionMedicamentos.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Intermediarios.DTO
{
    [AutoMap(typeof(Request))]
    public class GetMyRequestsOutput : EntityDto
    {
        /// <summary>
        /// Propiedad para guardar la fecha de creacion de la solicitud
        /// </summary>
        public virtual DateTime Fecha { get; set; }

        /// <summary>
        /// Propiedad para guardar la direccion de almacenamiento de la imagen de la receta
        /// </summary>
        public virtual string RutaReceta { get; set; }

        /// <summary>
        /// Propiedad para guardar los medicamentos solicitados
        /// </summary>
        public virtual List<DrugsRequestDTO> Medicamentos { get; set; }

        /// <summary>
        /// Propiedad para guardar, en el caso que existieran, las observaciones registradas por los intermediarios
        /// </summary>
        public virtual List<CommentDTO> Observaciones { get; set; }

        /// <summary>
        /// Propiedad para guardar el estado en el que se encuentra la solicitud
        /// </summary>
        public virtual int Estado { get; set; }

        /// <summary>
        /// Propiedad para guardar la persona que registro la solicitud
        /// </summary>
        public virtual PersonDTO Persona { get; set; }

        /// <summary>
        /// Propiedad para guardar el intermediario cuando aprueba la solicitud
        /// </summary>
        public virtual Intermediary Intermediario { get; set; }


    }
}
