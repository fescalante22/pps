﻿using Abp.AutoMapper;
using DonacionMedicamentos.Requests;
using System.ComponentModel.DataAnnotations;

namespace DonacionMedicamentos.Intermediarios.DTO
{
    [AutoMapTo(typeof(Request))]
    public class ApproveRequestInput
    {
        [Required]
        public virtual int Id { get; set; }

        [Required]
        public virtual IntermediaryDTO Intermediario { get; set; }
    }
}
