﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DonacionMedicamentos.Donations;
using DonacionMedicamentos.Drugs.DTO;
using DonacionMedicamentos.Personas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Intermediarios.DTO
{
    [AutoMap(typeof(Donation))]
    public class GetMyDonationsOutput : EntityDto
    {
        public DateTime Fecha { get; set; }

        public virtual List<DrugsDonationDTO> Medicamentos { get; set; }

        public virtual PersonIntermediaryOutput Persona { get; set; }
    }
}
