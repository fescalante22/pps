﻿using Abp.AutoMapper;
using DonacionMedicamentos.Donations;
using System.ComponentModel.DataAnnotations;

namespace DonacionMedicamentos.Intermediarios.DTO
{
    [AutoMapTo(typeof(Donation))]
    public class ApproveDonationInput
    {
        [Required]
        public virtual int Id { get; set; }

        [Required]
        public virtual IntermediaryDTO Intermediario { get; set; }
    }
}
