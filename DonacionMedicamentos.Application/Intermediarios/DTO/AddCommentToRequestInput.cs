﻿using Abp.AutoMapper;
using DonacionMedicamentos.Comments;
using DonacionMedicamentos.Requests;
using System.ComponentModel.DataAnnotations;

namespace DonacionMedicamentos.Intermediarios.DTO
{
    [AutoMap(typeof(Request))]
    public class AddCommentToRequestInput
    {
        [Required]
        public virtual int Id { get; set; }

        [Required]
        public virtual CommentDTO Comment { get; set; }
    }
}
