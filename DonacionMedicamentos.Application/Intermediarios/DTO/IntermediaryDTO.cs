﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DonacionMedicamentos.Locations.DTO;
using DonacionMedicamentos.Phones;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DonacionMedicamentos.Intermediarios.DTO
{
    [AutoMap(typeof(Intermediary))]
    public class IntermediaryDTO : EntityDto
    {
        [Required]
        public virtual string Account { get; set; }

        public virtual string Email { get; set; }

        public virtual string Password { get; set; }

        public virtual string Nombre { get; set; }

        public virtual string Calle { get; set; }

        public virtual int Numero { get; set; }

        public virtual int Piso { get; set; }

        public virtual int Departamento { get; set; }

        public virtual CityDTO Ciudad { get; set; }

        public virtual List<PhoneDTO> Telefonos { get; set; }
    }
}
