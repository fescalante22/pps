﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DonacionMedicamentos.Intermediarios.DTO;
using DonacionMedicamentos.Requests.DTO;
using DonacionMedicamentos.Donations.DTO;

namespace DonacionMedicamentos.Intermediarios
{
    public interface IIntermediaryAppService : IApplicationService
    {
        /// <summary>
        /// Método de interfaz para aprobar una donación
        /// </summary>
        void ApproveDonation(ApproveDonationInput input);

        /// <summary>
        /// Método de interfaz para aprobar una solicitud
        /// </summary>
        void ApproveRequest(ApproveRequestInput input);

        /// <summary>
        /// Método de interfaz para agregar una observación a una solicitud
        /// </summary>
        void AddCommentToRequest(AddCommentToRequestInput input);

        /// <summary>
        /// Obtiene todas las solicitudes
        /// </summary>
        List<GetRequestsOutput> GetAllRequests(IntermediaryDTO input);

        /// <summary>
        /// Metodo para obtener todas las solicitudes relacionadas al intermediario
        /// </summary>
        /// <returns></returns>
        List<GetMyRequestsOutput> GetMyRequests(IntermediaryDTO input);

        /// <summary>
        /// Metodo para obtener todas las donaciones relacionadas al intermediario 
        /// </summary>
        /// <returns></returns>
        List<GetMyDonationsOutput> GetMyDonations(IntermediaryDTO input);

        /// <summary>
        /// Obtiene todas las Donaciones
        /// </summary>
        List<GetDonationsOutput> GetAllDonations(IntermediaryDTO input);

        /// <summary>
        /// Metodo para modificar un Intermediario
        /// </summary>
        void UpdateIntermediary(UpdateIntermediaryInput input);

        /// <summary>
        /// Método de interfaz para obtener un intermediario por su Account
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        IntermediaryDTO GetByAccount(IntermediaryDTO input);

        /// <summary>
        /// Método para obtener un intermediario
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        IntermediaryDTO Get(IntermediaryDTO input);


        /// <summary>
        /// Métoo para obtener una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        /// <param name="pIntermediary"></param>
        /// <returns></returns>
        GetRequestsOutput GetRequest(GetRequestsInput input);


        /// <summary>
        /// Método para obtener una donación
        /// </summary>
        /// <param name="pDonation"></param>
        /// <param name="pIntermediary"></param>
        /// <returns></returns>
        GetDonationsOutput GetDonation(GetDonationsInput input);

        /// <summary>
        /// Método de interfaz para cambiar el estado de una donación
        /// </summary>
        /// <param name=""></param>
        void ChangeDonationState(ChangeDonationStateInput input);

        /// <summary>
        /// Método de interfaz para cambiar el estado de una solicitud
        /// </summary>
        /// <param name=""></param>
        void ChangeRequestState(ChangeRequestStateInput input);

    }
}
