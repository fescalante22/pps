﻿using System.Collections.Generic;
using DonacionMedicamentos.Intermediarios.DTO;
using DonacionMedicamentos.Requests;
using DonacionMedicamentos.Comments;
using DonacionMedicamentos.Donations;
using Abp.ObjectMapping;
using DonacionMedicamentos.Personas;
using System;
using DonacionMedicamentos.Cities;
using DonacionMedicamentos.Requests.DTO;
using DonacionMedicamentos.Donations.DTO;
using DonacionMedicamentos.Locations.DTO;
using DonacionMedicamentos.RequestStates;
using DonacionMedicamentos.DonationStates;

namespace DonacionMedicamentos.Intermediarios
{
    /// <summary>
    /// Servicio de Aplicacion del Intermediario
    /// </summary>
    public class IntermediaryAppService : IIntermediaryAppService
    {
        //Instanciamos IObjectMapper para realizar el mapeo de los objetos
        private readonly IObjectMapper _objectMapper;
        //Instanciamos los managers necesarios para delegar la logica
        private readonly IRequestManager _requestManager;
        private readonly IDonationManager _donationManager;
        private readonly ICommentManager _commentManager;
        private readonly IIntermediaryManager _intermediaryManager;
        private readonly IPersonManager _personManager;
        private readonly ICityManager _cityManager;


        /// <summary>
        /// Constructor del servicio de aplicacion de Usuario
        /// </summary>
        public IntermediaryAppService(
            IRequestManager requestManager,
            ICommentManager commentManager,
            IDonationManager donationManager,
            IIntermediaryManager intermediaryManager,
            IPersonManager personManager,
            IObjectMapper objectMapper,
            ICityManager cityManager)
        {
            _requestManager = requestManager;
            _commentManager = commentManager;
            _donationManager = donationManager;
            _intermediaryManager = intermediaryManager;          
            _personManager = personManager;
            _objectMapper = objectMapper;
            _cityManager = cityManager;
        }

        /// <summary>
        /// Método para agregar una observación a una solicitud
        /// </summary>
        /// <param name="input"></param>
        public void AddCommentToRequest(AddCommentToRequestInput input)
        {
            Request solicitud = this._requestManager.Get(this._objectMapper.Map<Request>(input));
            this._requestManager.AddComment(solicitud, this._objectMapper.Map<Comment>(input.Comment));
        }

        /// <summary>
        /// Metodo para aprobar una donacion
        /// </summary>
        /// <param name="input"></param>
        public void ApproveDonation(ApproveDonationInput input)
        {
            Donation donacion = this._donationManager.Get(this._objectMapper.Map<Donation>(input));
            Intermediary intermediario = this._intermediaryManager.Get(this._objectMapper.Map<Intermediary>(input.Intermediario));
            this._donationManager.Approve(donacion, intermediario);
        }

        /// <summary>
        /// Metodo para aprobar una solicitud
        /// </summary>
        /// <param name="input"></param>
        public void ApproveRequest(ApproveRequestInput input)
        {
            Request solicitud = this._requestManager.Get(this._objectMapper.Map<Request>(input));
            Intermediary intermediario = this._intermediaryManager.Get(this._objectMapper.Map<Intermediary>(input.Intermediario));
            this._requestManager.Approve(solicitud, intermediario);
        }

        /// <summary>
        /// Metodo para obtener todas las donaciones que no han sido aceptadas
        /// </summary>
        /// <returns></returns>
        public List<GetDonationsOutput> GetAllDonations(IntermediaryDTO input)
        {
            Intermediary intermediary = new Intermediary { Id = input.Id };
            return this._objectMapper.Map<List<GetDonationsOutput>>(this._donationManager.GetDonations().FindAll(x => (x.Intermediario == intermediary || x.Estado == DonationState.Registrada) && (x.Estado!= DonationState.Cancelada)));
        }

        /// <summary>
        /// Metodo para obtener todas las solicitudes que no han sido aceptadas
        /// </summary>
        /// <returns></returns>
        public List<GetRequestsOutput> GetAllRequests(IntermediaryDTO input)
        {
            Intermediary intermediary = new Intermediary { Id = input.Id };
            return this._objectMapper.Map<List<GetRequestsOutput>>(this._requestManager.GetRequests().FindAll(x=> (x.Intermediario == intermediary || x.Estado==RequestState.Registrada) && (x.Estado != RequestState.Cancelada)));
        }

        /// <summary>
        /// Metodo para obtener todas las solicitudes relacionadas al intermediario
        /// </summary>
        /// <returns></returns>
        public List<GetMyRequestsOutput> GetMyRequests(IntermediaryDTO input)
        {
            return this._objectMapper.Map<List<GetMyRequestsOutput>>(this._requestManager.GetRequests(this._objectMapper.Map<Intermediary>(input)));
        }

        /// <summary>
        /// Metodo para obtener todas las donaciones relacionadas al intermediario
        /// </summary>
        /// <returns></returns>
        public List<GetMyDonationsOutput> GetMyDonations(IntermediaryDTO input)
        {
            return this._objectMapper.Map<List<GetMyDonationsOutput>>(this._donationManager.GetDonations(this._objectMapper.Map<Intermediary>(input)));
        }

        /// <summary>
        /// Metodo para modificar un Intermediario
        /// </summary>
        /// <param name="pIntermediary"></param>
        public void UpdateIntermediary(UpdateIntermediaryInput input)
        {
            input.Ciudad = this._objectMapper.Map<CityDTO>(this._cityManager.Get(this._objectMapper.Map<City>(input.Ciudad)));
            Intermediary Intermediary = this._intermediaryManager.Get(this._objectMapper.Map<Intermediary>(input));
            Intermediary.Ciudad = (this._cityManager.Get(this._objectMapper.Map<City>(input.Ciudad)));
            Intermediary = _objectMapper.Map(input, Intermediary);

            try
            {
                this._intermediaryManager.Update(Intermediary);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        /// <summary>
        /// Obtiene un intermediario por su cuenta.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public IntermediaryDTO GetByAccount(IntermediaryDTO input)
        {
            Intermediary intermediary = _objectMapper.Map<Intermediary>(input);
            return _objectMapper.Map<IntermediaryDTO>(this._intermediaryManager.GetByAccount(intermediary));
        }

        /// <summary>
        /// Obtiene un intermediario por su id.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public IntermediaryDTO Get(IntermediaryDTO input)
        {
            Intermediary intermediary = _objectMapper.Map<Intermediary>(input);
            return _objectMapper.Map<IntermediaryDTO>(this._intermediaryManager.Get(intermediary));
        }

        /// <summary>
        /// Obtiene una solicitud para el intermediario (si fue aprobada por el mismo o no tiene intermediario asociado)
        /// </summary>
        /// <param name="pRequest"></param>
        /// <returns></returns>
        public GetRequestsOutput GetRequest(GetRequestsInput input)
        {
            Intermediary intermediary = this._intermediaryManager.Get(this._objectMapper.Map<Intermediary>(input.Intermediario));
            return this._objectMapper.Map<GetRequestsOutput>(this._requestManager.Get(new Request { Id = input.Id },intermediary));
        }

        /// <summary>
        /// Obtiene una donacion para el intermediario (si fue aprobada por el mismo o no tiene intermediario asociado)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public GetDonationsOutput GetDonation(GetDonationsInput input)
        {
            Intermediary intermediary = this._intermediaryManager.Get(this._objectMapper.Map<Intermediary>(input.Intermediario));
            return this._objectMapper.Map<GetDonationsOutput>(this._donationManager.Get(new Donation { Id = input.Id }, intermediary));
        }

        /// <summary>
        /// Método para cambiar el estado de una donación
        /// </summary>
        /// <param name=""></param>
        public void ChangeDonationState(ChangeDonationStateInput input)
        {
            this._donationManager.ChangeDonationState(this._objectMapper.Map<Donation>(input));
        }

        /// <summary>
        /// Método para cambiar el estado de una solicitud
        /// </summary>
        /// <param name=""></param>
        public void ChangeRequestState(ChangeRequestStateInput input)
        {
            this._requestManager.ChangeRequestState(this._objectMapper.Map<Request>(input));
        }
    }
}
