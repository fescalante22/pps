﻿using Abp.AutoMapper;
using DonacionMedicamentos.DrugsDonations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Drugs.DTO
{
    [AutoMap(typeof(DrugsDonation))]
    public class DrugsDonationDTO
    {
        [Required]
        public virtual int Cantidad { get; set; }

        [Required]
        public virtual DrugDTO Medicamento { get; set; }

        [Required]
        public virtual DateTime FechaVencimiento { get; set; }
    }
}
