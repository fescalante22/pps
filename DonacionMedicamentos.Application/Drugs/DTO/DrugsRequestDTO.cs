﻿using Abp.AutoMapper;
using DonacionMedicamentos.Drugs.DTO;
using DonacionMedicamentos.DrugsRequests;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Drugs.DTO
{
    [AutoMap(typeof(DrugsRequest))]
    public class DrugsRequestDTO
    {
        [Required]
        public virtual int Cantidad { get; set; }

        [Required]
        public virtual DrugDTO Medicamento { get; set; }
    }
}
