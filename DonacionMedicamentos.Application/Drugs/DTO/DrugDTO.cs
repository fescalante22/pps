﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DonacionMedicamentos.Drugs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Drugs.DTO
{
    [AutoMap(typeof(Drug))]
    public class DrugDTO : EntityDto
    {
        [Required]
        public virtual string NombreGenerico { get; set; }
        
        public virtual string NombreComercial { get; set; }

        public virtual string Dosis { get; set; }
    }
}
