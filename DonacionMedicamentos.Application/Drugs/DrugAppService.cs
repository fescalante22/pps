﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DonacionMedicamentos.Drugs.DTO;
using Abp.ObjectMapping;

namespace DonacionMedicamentos.Drugs
{
    public class DrugAppService : IDrugAppService
    {
        private readonly IDrugManager _drugManager;
        private readonly IObjectMapper _objectMapper;

        public DrugAppService(IDrugManager drugManager, IObjectMapper objectMapper)
        {
            _drugManager = drugManager;
            _objectMapper = objectMapper;
        }

        /// <summary>
        /// Obtengo todos los medicamentos
        /// </summary>
        /// <returns></returns>
        public List<DrugDTO> GetAll()
        {
            return this._objectMapper.Map<List<DrugDTO>>(this._drugManager.GetAll());
        }


        /// <summary>
        /// Obtengo un medicamento
        /// </summary>
        /// <returns></returns>
        public List<DrugDTO> Get()
        {
            //COMPLETAR
           return this._objectMapper.Map<List<DrugDTO>>(this._drugManager.GetAll());
        }

    }
}
