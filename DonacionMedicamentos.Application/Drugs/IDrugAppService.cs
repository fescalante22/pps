﻿using Abp.Application.Services;
using DonacionMedicamentos.Drugs.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Drugs
{
    public interface IDrugAppService : IApplicationService
    {
        List<DrugDTO> GetAll();
    }
}
