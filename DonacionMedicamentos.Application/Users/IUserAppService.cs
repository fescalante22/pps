﻿using Abp.Application.Services;
using DonacionMedicamentos.Users.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Users
{
    /// <summary>
    /// Interface del servicio de aplicacion de usuario
    /// </summary>
    public interface IUserAppService : IApplicationService
    {
        /// <summary>
        /// Metodo para cambiar la Password de un Usuario
        /// </summary>
        /// <param name="input"></param>
        void ChangePassUser(ChangePassUserInput input);

        /// <summary>
        /// Metodo para crear un Usuario
        /// </summary>
        /// <param name="input"></param>
        void CreateUser(CreateUserInput input);

        /// <summary>
        /// Metodo para que el Usuario inicie sesion
        /// </summary>
        /// <param name="input"></param>
        bool LoginUser(LoginUserInput input);

        /// <summary>
        /// Metodo para recuperar la Password de un Usuario
        /// </summary>
        /// <param name="input"></param>
        void RecoverPassUser(RecoverPassUserInput input);

        /// <summary>
        /// Metodo para realizar la baja logica de un Usuario
        /// </summary>
        /// <param name="input"></param>
        void Unregister(UnregisterUserInput input);

        /// <summary>
        /// Obtiene un usuario
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        UserDTO Get(UserDTO pUser);
        
    }
}
