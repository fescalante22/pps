﻿using Abp.ObjectMapping;
using DonacionMedicamentos.Mailing;
using DonacionMedicamentos.Users.DTO;
using System;

namespace DonacionMedicamentos.Users
{
    /// <summary>
    /// Servicio de Aplicacion de Usuario
    /// </summary>
    public class UserAppService : IUserAppService
    {
        //Instanciamos IObjectMapper para realizar el mapeo de los objetos
        private readonly IObjectMapper _objectMapper;
        //Instanciamos los managers necesarios para delegar la logica
        private readonly IUserManager _userManager;
        private readonly MailTrapMailing _mailTrap;


        /// <summary>
        /// Constructor del servicio de aplicacion de Usuario
        /// </summary>
        public UserAppService(
            IUserManager userManager,
            IObjectMapper objectMapper,
            MailTrapMailing mailTrap)
        {
            _userManager = userManager;
            _objectMapper = objectMapper;
            _mailTrap = mailTrap;
        }

        /// <summary>
        /// Metodo para cambiar la Password del usuario
        /// </summary>
        /// <param name="input"></param>
        public void ChangePassUser(ChangePassUserInput input)
        {
            this._userManager.ChangePass((this._objectMapper.Map<User>(input)), input.PasswordNueva);
        }

        /// <summary>
        /// Metodo para crear un Usuario
        /// </summary>
        /// <param name="input"></param>
        public void CreateUser(CreateUserInput input)
        {
            this._userManager.Create((this._objectMapper.Map<User>(input)));
        }

        public UserDTO Get(UserDTO pUser)
        {
            return (this._objectMapper.Map<UserDTO>(this._userManager.Get(this._objectMapper.Map<User>(pUser))));
        }

        /// <summary>
        /// Metodo para que un Usuario inicie sesion
        /// </summary>
        /// <param name="input"></param>
        public bool LoginUser(LoginUserInput input)
        {
            try
            {
                return this._userManager.Login((this._objectMapper.Map<User>(input)));
            }
            catch (Exception)
            {
                throw new Exception("Usuario o contraseña incorrecta");
            }
        }

        /// <summary>
        /// Metodo para cambiar la Password de un Usuario
        /// </summary>
        /// <param name="input"></param>
        public void RecoverPassUser(RecoverPassUserInput input)
        {
            User _user = this._userManager.GetByAccount(this._objectMapper.Map<User>(input));
            string _body = "Su contraseña es:" + _user.Password;
            this._mailTrap.SendEmail(_user.Email, "Donacion Medicamentos - Recuperacion de contraseña", _body);
        }

        /// <summary>
        /// Metodo para realizar la baja logica de un Usuario
        /// </summary>
        /// <param name="input"></param>
        public void Unregister(UnregisterUserInput input)
        {
            User user = this._objectMapper.Map<User>(input);
            this._userManager.Delete(this._userManager.Get(user));
        }
    }
}
