﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Users.DTO
{
    [AutoMap(typeof(User))]
    public class UserDTO : EntityDto
    {
        public string Account { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public DateTime FechaRegistro { get; set; }
    }
}
