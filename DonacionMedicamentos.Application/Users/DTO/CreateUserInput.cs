﻿using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace DonacionMedicamentos.Users.DTO
{
    [AutoMapTo(typeof(User))]
    public class CreateUserInput
    {
        [Required]
        public virtual string Account { get; set; }

        public virtual string Email { get; set; }

        [Required]
        public virtual string Password { get; set; }
    }
}
