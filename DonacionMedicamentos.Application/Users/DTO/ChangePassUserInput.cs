﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Users.DTO
{
    [AutoMap(typeof(User))]
    public class ChangePassUserInput : EntityDto
    {
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        public string PasswordNueva { get; set; }

        [DataType(DataType.Password)]
        public string RepeatPasswordNueva { get; set; }
    }
}
