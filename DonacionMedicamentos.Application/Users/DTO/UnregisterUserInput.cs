﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Users.DTO
{
    [AutoMapTo(typeof(User))]
    public class UnregisterUserInput
    {
        [Required]
        public virtual int Id { get; set; }
    }
}
