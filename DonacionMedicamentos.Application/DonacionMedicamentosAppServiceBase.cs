﻿using Abp.Application.Services;

namespace DonacionMedicamentos
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class DonacionMedicamentosAppServiceBase : ApplicationService
    {
        protected DonacionMedicamentosAppServiceBase()
        {
            LocalizationSourceName = DonacionMedicamentosConsts.LocalizationSourceName;
        }
    }
}