﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Phones
{
    [AutoMap(typeof(Phone))]
    public class PhoneDTO : EntityDto
    {
        public virtual int CodigoArea { get; set; }

        public virtual int Numero { get; set; }
    }
}
