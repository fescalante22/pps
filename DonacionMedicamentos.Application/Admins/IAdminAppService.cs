﻿using Abp.Application.Services;
using DonacionMedicamentos.Admins.DTO;
using DonacionMedicamentos.Intermediarios.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Admins
{
    /// <summary>
    /// Interfaz del Servicio de Aplicacion de Administrador
    /// </summary>
    public interface IAdminAppService : IApplicationService
    {
        /// <summary>
        /// Metodo de interfaz para crear un Administrador
        /// </summary>
        /// <param name="input"></param>
        void CreateAdmin(CreateAdminInput input);

        /// <summary>
        /// Metodo de interfaz para modificar un Administrador
        /// </summary>
        /// <param name="input"></param>
        void UpdateAdmin(UpdateAdminInput input);

        /// <summary>
        /// Metodo de interfaz para que un Administrador registre un Intermediario
        /// </summary>
        /// <param name="input"></param>
        void RegisterIntermediary(RegisterIntermediaryInput input);

        /// <summary>
        /// Metodo de interfaz para que un Administrador realice la baja lógica de un Intermediario
        /// </summary>
        /// <param name="input"></param>
        void UnRegisterIntermediary(UnregisterIntermediaryInput input);

        /// <summary>
        /// Metodo de interfaz para obtener todos los intermediarios registrados
        /// </summary>
        /// <returns></returns>
        List<IntermediaryDTO> GetAllIntermediaries();

        /// <summary>
        /// Obtiene un administrador por su cuenta.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        AdminDTO GetByAccount(AdminDTO input);
      
    }
}
