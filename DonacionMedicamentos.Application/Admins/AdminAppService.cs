﻿using System;
using System.Collections.Generic;
using Abp.ObjectMapping;
using DonacionMedicamentos.Admins.DTO;
using DonacionMedicamentos.Cities;
using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Intermediarios.DTO;
using DonacionMedicamentos.Requests;
using DonacionMedicamentos.Donations;

namespace DonacionMedicamentos.Admins
{
    /// <summary>
    /// Servicio de Aplicacion de Administrador
    /// </summary>
    public class AdminAppService : IAdminAppService
    {
        //Instanciamos IObjectMapper para realizar el mapeo de los objetos
        private readonly IObjectMapper _objectMapper;
        //Instanciamos los managers necesarios para delegar la logica
        private readonly IAdminManager _adminManager;
        private readonly ICityManager _cityManager;
        private readonly IIntermediaryManager _intermediaryManager;
        private readonly IRequestManager _requestManager;
        private readonly IDonationManager _donationManager;

        /// <summary>
        /// Constructor del servicio de aplicacion de Administrador
        /// </summary>
        public AdminAppService(
            IAdminManager adminManager, 
            IIntermediaryManager intermediaryManager, 
            IObjectMapper objectMapper,
            ICityManager cityManager,
            IRequestManager requestManager,
            IDonationManager donationManager)
        {
            _adminManager = adminManager;
            _intermediaryManager = intermediaryManager;
            _objectMapper = objectMapper;
            _cityManager = cityManager;
            _requestManager = requestManager;
            _donationManager = donationManager;
        }

        /// <summary>
        /// Metodo para registrar un Administrador
        /// </summary>
        /// <param name="input"></param>
        public void CreateAdmin(CreateAdminInput input)
        {
            this._adminManager.Create(this._objectMapper.Map<Admin>(input));
        }

        /// <summary>
        /// Metodo para que un Administrador registre un Intermediario
        /// </summary>
        /// <param name="input"></param>
        public void RegisterIntermediary(RegisterIntermediaryInput input)
        {
            City city = this._cityManager.Get(this._objectMapper.Map<City>(input.Ciudad));
            Intermediary intermediary = this._objectMapper.Map<Intermediary>(input);
            intermediary.Ciudad = city;
            this._intermediaryManager.Create(intermediary);
        }

        /// <summary>
        /// Metodo para que un Administrador realice la baja lógica de un Intermediario
        /// </summary>
        /// <param name="input"></param>
        public void UnRegisterIntermediary(UnregisterIntermediaryInput input)
        {
            Intermediary intermediary = this._intermediaryManager.Get(this._objectMapper.Map<Intermediary>(input));
            var solicitudes = this._requestManager.GetRequests(intermediary);
            var donaciones = this._donationManager.GetDonations(intermediary);
            this._intermediaryManager.Delete(intermediary);
            foreach (var solicitud in solicitudes)
            {
                this._requestManager.Disassociate(solicitud);
            }
            foreach (var donacion in donaciones)
            {
                this._donationManager.Disassociate(donacion);
            }
        }

        /// <summary>
        /// Metodo para modificar un Adminsitrador
        /// </summary>
        /// <param name="input"></param>
        public void UpdateAdmin(UpdateAdminInput input)
        {
            this._adminManager.Update(this._objectMapper.Map<Admin>(input));
        }

        /// <summary>
        /// Metodo para obtener todos los intermediarios registrados
        /// </summary>
        /// <returns></returns>
        public List<IntermediaryDTO> GetAllIntermediaries()
        {
            return this._objectMapper.Map<List<IntermediaryDTO>>(this._intermediaryManager.GetAll());
        }

        /// <summary>
        /// Obtiene un administrador por su cuenta.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public AdminDTO GetByAccount(AdminDTO input)
        {
            Admin admin = _objectMapper.Map<Admin>(input);
            return _objectMapper.Map<AdminDTO>(this._adminManager.GetByAccount(admin));
        }
    }
}
