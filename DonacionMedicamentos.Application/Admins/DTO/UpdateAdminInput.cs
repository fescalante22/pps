﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Admins.DTO
{
    [AutoMapTo(typeof(Admin))]
    public class UpdateAdminInput
    {
        [Required]
        public virtual string Account { get; set; }

        public virtual string Email { get; set; }

        public virtual string Password { get; set; }

        public virtual string Nombre { get; set; }
    }
}
