﻿using Abp.AutoMapper;
using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Locations.DTO;
using DonacionMedicamentos.Phones;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DonacionMedicamentos.Admins.DTO
{
    [AutoMap(typeof(Intermediary))]
    public class UnregisterIntermediaryInput
    {
        [Required]
        public virtual int Id { get; set; }

        public virtual string Account { get; set; }
    }
}
