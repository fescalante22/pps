﻿using Abp.AutoMapper;
using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Locations.DTO;
using DonacionMedicamentos.Phones;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Admins.DTO
{
    [AutoMap(typeof(Intermediary))]
    public class RegisterIntermediaryInput
    {
        [Required]
        public virtual string Account { get; set; }

        public virtual string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public virtual string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public virtual string RepeatPassword { get; set; }

        [Required]
        public virtual string Nombre { get; set; }

        public virtual string Calle { get; set; }

        public virtual string Numero { get; set; }

        public virtual string Piso { get; set; }

        public virtual string Departamento { get; set; }

        public virtual CityDTO Ciudad { get; set; }

        public virtual List<PhoneDTO> Telefonos { get; set; }
    }
}
