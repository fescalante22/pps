﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Admins.DTO
{
    [AutoMap(typeof(Admin))]
    public class CreateAdminInput
    {
        [Required]
        public virtual string Account { get; set; }

        public virtual string Email { get; set; }

        [Required]
        public virtual string Password { get; set; }

        [Required]
        public virtual string Nombre { get; set; }
    }
}
