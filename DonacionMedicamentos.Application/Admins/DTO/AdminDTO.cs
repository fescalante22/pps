﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace DonacionMedicamentos.Admins.DTO
{
    [AutoMap(typeof(Admin))]
    public class AdminDTO : EntityDto
    {
        [Required]
        public virtual string Account { get; set; }

        public virtual string Password { get; set; }

        public virtual string Email { get; set; }

        public virtual string Nombre { get; set; }
    }
}
