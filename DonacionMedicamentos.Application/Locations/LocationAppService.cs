﻿using System.Collections.Generic;
using DonacionMedicamentos.Cities;
using DonacionMedicamentos.States;
using DonacionMedicamentos.Locations.DTO;
using Abp.ObjectMapping;

namespace DonacionMedicamentos.Locations
{
    public class LocationAppService : ILocationAppService
    {
        //Instanciamos IObjectMapper para realizar el mapeo de los objetos
        private readonly IObjectMapper _objectMapper;
        //Instanciamos los managers necesarios para delegar la logica
        private readonly ICityManager _cityManager;
        private readonly IStateManager _stateManager;


        /// <summary>
        /// Constructor del servicio de aplicacón de Location
        /// </summary>
        /// <param name="cityManager"></param>
        /// <param name="stateManager"></param>
        /// <param name="objectMapper"></param>
        public LocationAppService(
            ICityManager cityManager, 
            IStateManager stateManager, 
            IObjectMapper objectMapper )
        {
            this._cityManager = cityManager;
            this._stateManager = stateManager;
            this._objectMapper = objectMapper;
        }

        /// <summary>
        /// Método para obtener todos los estados
        /// </summary>
        /// <returns></returns>
        public List<StateDTO> GetAllStates()
        {
            return this._objectMapper.Map<List<StateDTO>>(this._stateManager.GetAll());
        }

        /// <summary>
        /// Obtiene todas las ciudades de la provincia ingresada
        /// </summary>
        /// <param name="pState"></param>
        /// <returns></returns>
        public List<CityDTO> GetAllCities(StateDTO pState)
        {
            State state = this._objectMapper.Map<State>(pState);
            return this._objectMapper.Map<List<CityDTO>>(this._cityManager.GetAll(state));
        }
    }
}
