﻿using Abp.Application.Services;
using DonacionMedicamentos.Locations.DTO;
using System.Collections.Generic;

namespace DonacionMedicamentos.Locations
{
    public interface ILocationAppService : IApplicationService
    {
        List<StateDTO> GetAllStates();

        List<CityDTO> GetAllCities(StateDTO pState);
    }
}
