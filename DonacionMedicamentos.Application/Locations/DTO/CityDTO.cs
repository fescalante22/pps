﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DonacionMedicamentos.Cities;
using DonacionMedicamentos.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Locations.DTO
{
    [AutoMap(typeof(City))]
    public class CityDTO : EntityDto
    {
        /// <summary>
        /// Propiedad de la ciudad para guardar su ccodigo postal
        /// </summary>
        public virtual int CodigoPostal { get; set; }

        /// <summary>
        /// Propiedad de la ciudad para guardar su nombre
        /// </summary>
        public virtual string Nombre { get; set; }

        /// <summary>
        /// Propiedad de la ciudad para guardar la provincia a la que pertenece
        /// </summary>
        public virtual StateDTO Provincia { get; set; }

    
    }
}
