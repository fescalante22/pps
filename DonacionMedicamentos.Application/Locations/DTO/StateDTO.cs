﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DonacionMedicamentos.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Locations.DTO
{
    [AutoMap(typeof(State))]
    public class StateDTO : EntityDto
    {
        public virtual string Nombre { get; set; }
    }
}
