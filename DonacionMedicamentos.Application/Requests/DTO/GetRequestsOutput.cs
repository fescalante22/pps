﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using DonacionMedicamentos.Requests;
using Abp.Application.Services.Dto;
using DonacionMedicamentos.Drugs.DTO;
using DonacionMedicamentos.Personas.DTO;
using DonacionMedicamentos.Comments;
using DonacionMedicamentos.Intermediarios.DTO;

namespace DonacionMedicamentos.Requests.DTO
{
    [AutoMap(typeof(Request))]
    public class GetRequestsOutput : EntityDto
    {
        /// <summary>
        /// Propiedad para guardar la fecha de creacion de la solicitud
        /// </summary>
        public DateTime Fecha { get; set; }

        /// <summary>
        /// Propiedad para guardar la direccion de almacenamiento de la imagen de la receta
        /// </summary>
        public string RutaReceta { get; set; }

        /// <summary>
        /// Propiedad para guardar los medicamentos solicitados
        /// </summary>
        public List<DrugsRequestDTO> Medicamentos { get; set; }

        /// <summary>
        /// Propiedad para guardar, en el caso que existieran, las observaciones registradas por los intermediarios
        /// </summary>
        public virtual List<CommentDTO> Observaciones { get; set; }

        /// <summary>
        /// Propiedad para guardar el estado en el que se encuentra la solicitud
        /// </summary>
        public virtual string Estado { get; set; }

        /// <summary>
        /// Propiedad para guardar la persona que registro la solicitud
        /// </summary>
        public virtual PersonDTO Persona { get; set; }

        /// <summary>
        /// Propiedad para guardar el intermediario que aprobo la solicitud
        /// </summary>
        public virtual IntermediaryDTO Intermediario { get; set; }

    }
}
