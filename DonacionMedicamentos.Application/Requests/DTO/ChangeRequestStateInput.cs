﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DonacionMedicamentos.Donations;
using DonacionMedicamentos.Drugs.DTO;
using DonacionMedicamentos.Intermediarios.DTO;
using DonacionMedicamentos.Personas.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DonacionMedicamentos.Requests.DTO
{
    [AutoMap(typeof(Request))]
    public class ChangeRequestStateInput : EntityDto
    {
        public virtual int Estado { get; set; }

    }
}
