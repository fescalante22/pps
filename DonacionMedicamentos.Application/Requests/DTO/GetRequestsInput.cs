﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using DonacionMedicamentos.Requests;
using Abp.Application.Services.Dto;
using DonacionMedicamentos.Drugs.DTO;
using DonacionMedicamentos.Personas.DTO;
using DonacionMedicamentos.Comments;
using DonacionMedicamentos.Intermediarios.DTO;

namespace DonacionMedicamentos.Requests.DTO
{
    [AutoMap(typeof(Request))]
    public class GetRequestsInput : EntityDto
    {

        /// <summary>
        /// Propiedad para guardar la persona que registro la solicitud
        /// </summary>
        public virtual PersonDTO Persona { get; set; }

        /// <summary>
        /// Propiedad para guardar el intermediario que aprobo la solicitud
        /// </summary>
        public virtual IntermediaryDTO Intermediario { get; set; }

    }
}
