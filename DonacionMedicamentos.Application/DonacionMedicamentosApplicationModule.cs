﻿using System.Reflection;
using Abp.Modules;
using Abp.AutoMapper;
using DonacionMedicamentos.Intermediarios.DTO;
using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Locations.DTO;
using DonacionMedicamentos.Cities;

namespace DonacionMedicamentos
{
    [DependsOn(typeof(DonacionMedicamentosCoreModule), typeof(AbpAutoMapperModule))]
    public class DonacionMedicamentosApplicationModule : AbpModule
    {

        public override void PreInitialize()
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(config =>
            {
                config.CreateMap<IntermediaryDTO, Intermediary>()
                      .ForMember(u => u.Ciudad, options => options.MapFrom(input => input.Ciudad));

                config.CreateMap<Intermediary, IntermediaryDTO>()
                      .ForMember(u => u.Ciudad, options => options.MapFrom(input => input.Ciudad));

                config.CreateMap<City, CityDTO>()
                      .ForMember(u => u.Provincia, options => options.MapFrom(input => input.Provincia));
                config.CreateMap<CityDTO, City>()
                      .ForMember(u => u.Provincia, options => options.MapFrom(input => input.Provincia));
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
