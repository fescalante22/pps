﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace DonacionMedicamentos.EntityFramework.Repositories
{
    public abstract class DonacionMedicamentosRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<DonacionMedicamentosDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected DonacionMedicamentosRepositoryBase(IDbContextProvider<DonacionMedicamentosDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class DonacionMedicamentosRepositoryBase<TEntity> : DonacionMedicamentosRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected DonacionMedicamentosRepositoryBase(IDbContextProvider<DonacionMedicamentosDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
