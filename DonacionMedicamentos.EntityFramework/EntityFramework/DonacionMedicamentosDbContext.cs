﻿using Abp.EntityFramework;
using System.Data.Common;
using System.Data.Entity;


namespace DonacionMedicamentos.EntityFramework
{
    public class DonacionMedicamentosDbContext : AbpDbContext
    {
        public virtual IDbSet<Admins.Admin> Admins { get; set; }
        public virtual IDbSet<Cities.City> Cities { get; set; }
        public virtual IDbSet<Comments.Comment> Comments { get; set; }
        public virtual IDbSet<Donations.Donation> Donations { get; set; }
        public virtual IDbSet<Drugs.Drug> Drugs { get; set; }
        public virtual IDbSet<DrugsDonations.DrugsDonation> DrugsDonations { get; set; }
        public virtual IDbSet<DrugsRequests.DrugsRequest> DrugsRequests { get; set; }              
        public virtual IDbSet<Intermediarios.Intermediary> Intermediarios { get; set; }
        public virtual IDbSet<Personas.Person> Personas { get; set; }
        public virtual IDbSet<Phones.Phone> Phones { get; set; }        
        public virtual IDbSet<Requests.Request> Requests { get; set; }
        public virtual IDbSet<States.State> States { get; set; }
        public virtual IDbSet<Users.User> Users { get; set; }


        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public DonacionMedicamentosDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in DonacionMedicamentosDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of DonacionMedicamentosDbContext since ABP automatically handles it.
         */
        public DonacionMedicamentosDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }
        

        //This constructor is used in tests
        public DonacionMedicamentosDbContext(DbConnection existingConnection)
         : base(existingConnection, true)
        {

        }

        public DonacionMedicamentosDbContext(DbConnection existingConnection, bool contextOwnsConnection)
         : base(existingConnection, contextOwnsConnection)
        {

        }
        
    }
}
