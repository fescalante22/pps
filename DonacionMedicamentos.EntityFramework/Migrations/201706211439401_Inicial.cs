namespace DonacionMedicamentos.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CodigoPostal = c.Int(nullable: false),
                        Nombre = c.String(),
                        Provincia_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.States", t => t.Provincia_Id)
                .Index(t => t.Provincia_Id);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Donations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        Estado_Id = c.Int(),
                        Person_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DonationStates", t => t.Estado_Id)
                .ForeignKey("dbo.Person", t => t.Person_Id)
                .Index(t => t.Estado_Id)
                .Index(t => t.Person_Id);
            
            CreateTable(
                "dbo.DonationStates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DrugsDonation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        FechaVencimiento = c.DateTime(nullable: false),
                        Medicamento_Id = c.Int(),
                        Donation_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Drugs", t => t.Medicamento_Id)
                .ForeignKey("dbo.Donations", t => t.Donation_Id)
                .Index(t => t.Medicamento_Id)
                .Index(t => t.Donation_Id);
            
            CreateTable(
                "dbo.Drugs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreGenerico = c.String(),
                        NombreComercial = c.String(),
                        Dosis = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Password = c.String(),
                        FechaRegistro = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_User_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        RutaReceta = c.String(),
                        Estado_Id = c.Int(),
                        Person_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RequestStates", t => t.Estado_Id)
                .ForeignKey("dbo.Person", t => t.Person_Id)
                .Index(t => t.Estado_Id)
                .Index(t => t.Person_Id);
            
            CreateTable(
                "dbo.RequestStates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DrugsRequest",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        Medicamento_Id = c.Int(),
                        Request_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Drugs", t => t.Medicamento_Id)
                .ForeignKey("dbo.Requests", t => t.Request_Id)
                .Index(t => t.Medicamento_Id)
                .Index(t => t.Request_Id);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        Detalle = c.String(),
                        Request_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Requests", t => t.Request_Id)
                .Index(t => t.Request_Id);
            
            CreateTable(
                "dbo.Phones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CodigoArea = c.Int(nullable: false),
                        Numero = c.Int(nullable: false),
                        Person_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.Person_Id)
                .Index(t => t.Person_Id);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Ciudad_Id = c.Int(),
                        Nombre = c.String(),
                        Apellido = c.String(),
                        Genero = c.String(),
                        Calle = c.String(),
                        Numero = c.Int(nullable: false),
                        Piso = c.Int(nullable: false),
                        Departamento = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Person_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Id)
                .ForeignKey("dbo.Cities", t => t.Ciudad_Id)
                .Index(t => t.Id)
                .Index(t => t.Ciudad_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Person", "Ciudad_Id", "dbo.Cities");
            DropForeignKey("dbo.Person", "Id", "dbo.Users");
            DropForeignKey("dbo.Phones", "Person_Id", "dbo.Person");
            DropForeignKey("dbo.Requests", "Person_Id", "dbo.Person");
            DropForeignKey("dbo.Comments", "Request_Id", "dbo.Requests");
            DropForeignKey("dbo.DrugsRequest", "Request_Id", "dbo.Requests");
            DropForeignKey("dbo.DrugsRequest", "Medicamento_Id", "dbo.Drugs");
            DropForeignKey("dbo.Requests", "Estado_Id", "dbo.RequestStates");
            DropForeignKey("dbo.Donations", "Person_Id", "dbo.Person");
            DropForeignKey("dbo.DrugsDonation", "Donation_Id", "dbo.Donations");
            DropForeignKey("dbo.DrugsDonation", "Medicamento_Id", "dbo.Drugs");
            DropForeignKey("dbo.Donations", "Estado_Id", "dbo.DonationStates");
            DropForeignKey("dbo.Cities", "Provincia_Id", "dbo.States");
            DropIndex("dbo.Person", new[] { "Ciudad_Id" });
            DropIndex("dbo.Person", new[] { "Id" });
            DropIndex("dbo.Phones", new[] { "Person_Id" });
            DropIndex("dbo.Comments", new[] { "Request_Id" });
            DropIndex("dbo.DrugsRequest", new[] { "Request_Id" });
            DropIndex("dbo.DrugsRequest", new[] { "Medicamento_Id" });
            DropIndex("dbo.Requests", new[] { "Person_Id" });
            DropIndex("dbo.Requests", new[] { "Estado_Id" });
            DropIndex("dbo.DrugsDonation", new[] { "Donation_Id" });
            DropIndex("dbo.DrugsDonation", new[] { "Medicamento_Id" });
            DropIndex("dbo.Donations", new[] { "Person_Id" });
            DropIndex("dbo.Donations", new[] { "Estado_Id" });
            DropIndex("dbo.Cities", new[] { "Provincia_Id" });
            DropTable("dbo.Person",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Person_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Phones");
            DropTable("dbo.Comments");
            DropTable("dbo.DrugsRequest");
            DropTable("dbo.RequestStates");
            DropTable("dbo.Requests");
            DropTable("dbo.Users",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_User_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Drugs");
            DropTable("dbo.DrugsDonation");
            DropTable("dbo.DonationStates");
            DropTable("dbo.Donations");
            DropTable("dbo.States");
            DropTable("dbo.Cities");
        }
    }
}
