// <auto-generated />
namespace DonacionMedicamentos.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Migracion11717 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Migracion11717));
        
        string IMigrationMetadata.Id
        {
            get { return "201707111206478_Migracion 11-7-17"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
