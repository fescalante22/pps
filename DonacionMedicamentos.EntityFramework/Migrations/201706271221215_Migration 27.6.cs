namespace DonacionMedicamentos.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Migration276 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Drugs", newName: "Drug");
            CreateTable(
                "dbo.Admin",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Nombre = c.String(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Admin_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Intermediary",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Nombre = c.String(),
                        Calle = c.String(),
                        Numero = c.String(),
                        Piso = c.String(),
                        Departamento = c.String(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Intermediary_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Intermediary", "Id", "dbo.Users");
            DropForeignKey("dbo.Admin", "Id", "dbo.Users");
            DropIndex("dbo.Intermediary", new[] { "Id" });
            DropIndex("dbo.Admin", new[] { "Id" });
            DropTable("dbo.Intermediary",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Intermediary_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Admin",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Admin_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            RenameTable(name: "dbo.Drug", newName: "Drugs");
        }
    }
}
