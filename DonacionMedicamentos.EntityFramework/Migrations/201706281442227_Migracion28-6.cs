namespace DonacionMedicamentos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migracion286 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Cities", newName: "City");
            RenameTable(name: "dbo.States", newName: "State");
            RenameTable(name: "dbo.Donations", newName: "Donation");
            RenameTable(name: "dbo.DonationStates", newName: "DonationState");
            RenameTable(name: "dbo.Users", newName: "User");
            RenameTable(name: "dbo.Requests", newName: "Request");
            RenameTable(name: "dbo.RequestStates", newName: "RequestState");
            RenameTable(name: "dbo.Comments", newName: "Comment");
            RenameTable(name: "dbo.Phones", newName: "Phone");
            RenameColumn(table: "dbo.Donation", name: "Person_Id", newName: "Persona_Id");
            RenameColumn(table: "dbo.Request", name: "Person_Id", newName: "Persona_Id");
            RenameIndex(table: "dbo.Donation", name: "IX_Person_Id", newName: "IX_Persona_Id");
            RenameIndex(table: "dbo.Request", name: "IX_Person_Id", newName: "IX_Persona_Id");
            AddColumn("dbo.DonationState", "Nombre", c => c.String());
            AddColumn("dbo.DonationState", "Descripcion", c => c.String());
            AddColumn("dbo.Intermediary", "Ciudad_Id", c => c.Int());
            AddColumn("dbo.RequestState", "Nombre", c => c.String());
            AddColumn("dbo.RequestState", "Descripcion", c => c.String());
            AddColumn("dbo.Phone", "Intermediary_Id", c => c.Int());
            AlterColumn("dbo.Intermediary", "Numero", c => c.Int(nullable: false));
            AlterColumn("dbo.Intermediary", "Piso", c => c.Int(nullable: false));
            AlterColumn("dbo.Intermediary", "Departamento", c => c.Int(nullable: false));
            CreateIndex("dbo.Phone", "Intermediary_Id");
            CreateIndex("dbo.Intermediary", "Ciudad_Id");
            AddForeignKey("dbo.Phone", "Intermediary_Id", "dbo.Intermediary", "Id");
            AddForeignKey("dbo.Intermediary", "Ciudad_Id", "dbo.City", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Intermediary", "Ciudad_Id", "dbo.City");
            DropForeignKey("dbo.Phone", "Intermediary_Id", "dbo.Intermediary");
            DropIndex("dbo.Intermediary", new[] { "Ciudad_Id" });
            DropIndex("dbo.Phone", new[] { "Intermediary_Id" });
            AlterColumn("dbo.Intermediary", "Departamento", c => c.String());
            AlterColumn("dbo.Intermediary", "Piso", c => c.String());
            AlterColumn("dbo.Intermediary", "Numero", c => c.String());
            DropColumn("dbo.Phone", "Intermediary_Id");
            DropColumn("dbo.RequestState", "Descripcion");
            DropColumn("dbo.RequestState", "Nombre");
            DropColumn("dbo.Intermediary", "Ciudad_Id");
            DropColumn("dbo.DonationState", "Descripcion");
            DropColumn("dbo.DonationState", "Nombre");
            RenameIndex(table: "dbo.Request", name: "IX_Persona_Id", newName: "IX_Person_Id");
            RenameIndex(table: "dbo.Donation", name: "IX_Persona_Id", newName: "IX_Person_Id");
            RenameColumn(table: "dbo.Request", name: "Persona_Id", newName: "Person_Id");
            RenameColumn(table: "dbo.Donation", name: "Persona_Id", newName: "Person_Id");
            RenameTable(name: "dbo.Phone", newName: "Phones");
            RenameTable(name: "dbo.Comment", newName: "Comments");
            RenameTable(name: "dbo.RequestState", newName: "RequestStates");
            RenameTable(name: "dbo.Request", newName: "Requests");
            RenameTable(name: "dbo.User", newName: "Users");
            RenameTable(name: "dbo.DonationState", newName: "DonationStates");
            RenameTable(name: "dbo.Donation", newName: "Donations");
            RenameTable(name: "dbo.State", newName: "States");
            RenameTable(name: "dbo.City", newName: "Cities");
        }
    }
}
