namespace DonacionMedicamentos.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class migracion2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Contraseña = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_User_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.City",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CodigoPostal = c.Int(nullable: false),
                        Nombre = c.String(),
                        Provincia_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.States", t => t.Provincia_Id)
                .Index(t => t.Provincia_Id);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Donations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Person_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.Person_Id)
                .Index(t => t.Person_Id);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Person_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.Person_Id)
                .Index(t => t.Person_Id);
            
            CreateTable(
                "dbo.Phone",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CodigoArea = c.Int(nullable: false),
                        Numero = c.Int(nullable: false),
                        Person_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.Person_Id)
                .Index(t => t.Person_Id);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Ciudad_Id = c.Int(),
                        Nombre = c.String(),
                        Apellido = c.String(),
                        Genero = c.String(),
                        Calle = c.String(),
                        Numero = c.Int(nullable: false),
                        Piso = c.Int(nullable: false),
                        Departamento = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Person_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Id)
                .ForeignKey("dbo.City", t => t.Ciudad_Id)
                .Index(t => t.Id)
                .Index(t => t.Ciudad_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Person", "Ciudad_Id", "dbo.City");
            DropForeignKey("dbo.Person", "Id", "dbo.Users");
            DropForeignKey("dbo.Phone", "Person_Id", "dbo.Person");
            DropForeignKey("dbo.Requests", "Person_Id", "dbo.Person");
            DropForeignKey("dbo.Donations", "Person_Id", "dbo.Person");
            DropForeignKey("dbo.City", "Provincia_Id", "dbo.States");
            DropIndex("dbo.Person", new[] { "Ciudad_Id" });
            DropIndex("dbo.Person", new[] { "Id" });
            DropIndex("dbo.Phone", new[] { "Person_Id" });
            DropIndex("dbo.Requests", new[] { "Person_Id" });
            DropIndex("dbo.Donations", new[] { "Person_Id" });
            DropIndex("dbo.City", new[] { "Provincia_Id" });
            DropTable("dbo.Person",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Person_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Phone");
            DropTable("dbo.Requests");
            DropTable("dbo.Donations");
            DropTable("dbo.States");
            DropTable("dbo.City");
            DropTable("dbo.Users",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_User_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
