namespace DonacionMedicamentos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatosNuevosPersona : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Donation", "Intermediario_Id", c => c.Int());
            AddColumn("dbo.Person", "FechaNacimiento", c => c.DateTime(nullable: false));
            AddColumn("dbo.Request", "Intermediario_Id", c => c.Int());
            CreateIndex("dbo.Donation", "Intermediario_Id");
            CreateIndex("dbo.Request", "Intermediario_Id");
            AddForeignKey("dbo.Donation", "Intermediario_Id", "dbo.Intermediary", "Id");
            AddForeignKey("dbo.Request", "Intermediario_Id", "dbo.Intermediary", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Request", "Intermediario_Id", "dbo.Intermediary");
            DropForeignKey("dbo.Donation", "Intermediario_Id", "dbo.Intermediary");
            DropIndex("dbo.Request", new[] { "Intermediario_Id" });
            DropIndex("dbo.Donation", new[] { "Intermediario_Id" });
            DropColumn("dbo.Request", "Intermediario_Id");
            DropColumn("dbo.Person", "FechaNacimiento");
            DropColumn("dbo.Donation", "Intermediario_Id");
        }
    }
}
