namespace DonacionMedicamentos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migracion11717 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Donation", "Estado_Id", "dbo.DonationState");
            DropForeignKey("dbo.Request", "Estado_Id", "dbo.RequestState");
            DropIndex("dbo.Donation", new[] { "Estado_Id" });
            DropIndex("dbo.Request", new[] { "Estado_Id" });
            AddColumn("dbo.Donation", "Estado", c => c.Int(nullable: false));
            AddColumn("dbo.Request", "Estado", c => c.Int(nullable: false));
            DropColumn("dbo.Donation", "Estado_Id");
            DropColumn("dbo.Request", "Estado_Id");
            DropTable("dbo.DonationState");
            DropTable("dbo.RequestState");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.RequestState",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DonationState",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Request", "Estado_Id", c => c.Int());
            AddColumn("dbo.Donation", "Estado_Id", c => c.Int());
            DropColumn("dbo.Request", "Estado");
            DropColumn("dbo.Donation", "Estado");
            CreateIndex("dbo.Request", "Estado_Id");
            CreateIndex("dbo.Donation", "Estado_Id");
            AddForeignKey("dbo.Request", "Estado_Id", "dbo.RequestState", "Id");
            AddForeignKey("dbo.Donation", "Estado_Id", "dbo.DonationState", "Id");
        }
    }
}
