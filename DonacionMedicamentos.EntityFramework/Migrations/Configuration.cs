using System.Data.Entity.Migrations;

namespace DonacionMedicamentos.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DonacionMedicamentos.EntityFramework.DonacionMedicamentosDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "DonacionMedicamentos";
        }

        protected override void Seed(DonacionMedicamentos.EntityFramework.DonacionMedicamentosDbContext context)
        {
            // This method will be called every time after migrating to the latest version.
            // You can add any seed data here...
        }
    }
}
