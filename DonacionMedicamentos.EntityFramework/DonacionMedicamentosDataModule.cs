﻿using System.Data.Entity;
using System.Reflection;
using Abp.EntityFramework;
using Abp.Modules;
using DonacionMedicamentos.EntityFramework;

namespace DonacionMedicamentos
{
    [DependsOn(typeof(AbpEntityFrameworkModule), typeof(DonacionMedicamentosCoreModule))]
    public class DonacionMedicamentosDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            Database.SetInitializer<DonacionMedicamentosDbContext>(null);
        }
    }
}
