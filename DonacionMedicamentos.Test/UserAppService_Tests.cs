﻿using Abp.ObjectMapping;
using DonacionMedicamentos.Users;
using DonacionMedicamentos.Users.DTO;
using Shouldly;
using System;
using System.Linq;
using Xunit;

namespace DonacionMedicamentos
{
    public class UserAppService_Tests : DonacionMedicamentosTestBase
    {
        //Instanciamos la clase a testear
        private readonly IUserAppService _userAppService;
        private readonly IObjectMapper _objectMapper;

        /// <summary>
        /// Constructor de la clase de testing para el Servicio de Aplicacion de Persona
        /// </summary>
        public UserAppService_Tests()
        {
            //Creamos la clase que se testea
            _userAppService = LocalIocManager.Resolve<IUserAppService>();
            _objectMapper = LocalIocManager.Resolve<IObjectMapper>();
        }

        [Fact]
        public void Should_Unregister_User()
        {
            //Seguimos el patron AAA (Arrange, Act, Assert)
            //ARRANGE - Preparacion
            var initialUserCount = UsingDbContext(context => context.Users.Count(x=>x.IsDeleted==false));

            //ACT - Corremos el SUT (Software Under Test)
            _userAppService.Unregister(
                new UnregisterUserInput
                {
                    Id = 54
                });

            //ASSERT - Comprobacion de resultados
            UsingDbContext(context =>
            {
                context.Users.Count(x => x.IsDeleted == false).ShouldBe(initialUserCount - 1);
            });
        }
    }
}
