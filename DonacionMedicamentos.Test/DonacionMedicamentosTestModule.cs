﻿using Abp.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos
{
    [DependsOn(
            typeof(DonacionMedicamentosDataModule),
            typeof(DonacionMedicamentosCoreModule),
            typeof(DonacionMedicamentosApplicationModule)
        )]
    public class DonacionMedicamentosTestModule : AbpModule
    {

    }
}
