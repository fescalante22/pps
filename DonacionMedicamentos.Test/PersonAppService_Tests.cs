﻿using Abp.ObjectMapping;
using DonacionMedicamentos.Locations.DTO;
using DonacionMedicamentos.Personas;
using DonacionMedicamentos.Personas.DTO;
using Shouldly;
using System;
using System.Linq;
using Xunit;

namespace DonacionMedicamentos
{
    public class PersonAppService_Tests : DonacionMedicamentosTestBase
    {/*
        //Instanciamos la clase a testear
        private readonly IPersonAppService _personAppService;
        private readonly IObjectMapper _objectMapper;

        /// <summary>
        /// Constructor de la clase de testing para el Servicio de Aplicacion de Persona
        /// </summary>
        public PersonAppService_Tests()
        {
            //Creamos la clase que se testea
            _personAppService = LocalIocManager.Resolve<IPersonAppService>();
            _objectMapper = LocalIocManager.Resolve<IObjectMapper>();
        }

        [Fact]
        public void Should_Create_New_Personas()
        {
            //Seguimos el patron AAA (Arrange, Act, Assert)
            //ARRANGE - Preparacion
            var initialPersonCount = UsingDbContext(context => context.Personas.Count());

            //ACT - Corremos el SUT (Software Under Test)
            _personAppService.CreatePerson(
                new CreatePersonInput
                {
                    Account = "36248636",
                    Password = "123456",
                    Nombre = "Santiago",
                    Apellido = "Tommasi",
                    Calle = "Galarza",
                    Numero = 1586,
                    Ciudad = new CityDTO { Id= 1,Nombre = "CdelU" },
                    FechaNacimiento = DateTime.Parse("1994-09-13")
        });

            //ASSERT - Comprobacion de resultados
            UsingDbContext(context =>
            {
                context.Personas.Count().ShouldBe(initialPersonCount + 1);
            });
        }

        [Fact]
        public void Should_Update_Persona()
        {
            //Seguimos el patron AAA (Arrange, Act, Assert)
            //ARRANGE - Preparacion
            var initialPerson = GetPerson("37563541");


            //ACT - Corremos el SUT (Software Under Test)
            this._personAppService.UpdatePerson(new UpdatePersonInput
            {
                Account = initialPerson.Account,
                Apellido = "Cosme Fulanito",
                Calle = initialPerson.Calle,
                Ciudad = new CityDTO { Id = 1 },
                Nombre = initialPerson.Nombre,
                Numero = initialPerson.Numero
            });
            

            //ASSERT - Comprobacion de resultados
            UsingDbContext(context =>
            {
                context.Personas.FirstOrDefault(x => x.Apellido == "Cosme Fulanito").ShouldNotBe(null);
            });
        }


        private Person GetPerson(string pAccount)
        {
            return UsingDbContext(context => context.Personas.FirstOrDefault(p => p.Account == pAccount));
        }
        */
    }
}
