﻿using Abp.TestBase;
using Castle.MicroKernel.Registration;
using DonacionMedicamentos.EntityFramework;
using EntityFramework.DynamicFilters;
using System;
using System.Data.Common;

namespace DonacionMedicamentos
{
    /// <summary>
    /// Clase base para todas las clases de test.
    /// It prepares ABP system, modules and a fake, in-memory database.
    /// Seeds database with initial data (<see cref="DonacionMedicamentosInitialDataBuilder"/>).
    /// Provides methods to easily work with DbContext.
    /// </summary>
    public abstract class DonacionMedicamentosTestBase : AbpIntegratedTestBase<DonacionMedicamentosTestModule>
    {
        protected DonacionMedicamentosTestBase()
        {
            UsingDbContext(context => new DonacionMedicamentosInitialDataBuilder().Build(context));
        }

        protected override void PreInitialize()
        {
            Effort.Provider.EffortProviderConfiguration.RegisterProvider();
            //Falsa DbConnection usando Effort
            LocalIocManager.IocContainer.Register(
                Component.For<DbConnection>()
                    .UsingFactoryMethod(Effort.DbConnectionFactory.CreateTransient)
                    .LifestyleSingleton()
                );
            base.PreInitialize();
        }

        public void UsingDbContext(Action<DonacionMedicamentosDbContext> action)
        {
            using (var context = LocalIocManager.Resolve<DonacionMedicamentosDbContext>())
            {
                context.DisableAllFilters();
                action(context);
                context.SaveChanges();
            }
        }

        public T UsingDbContext<T>(Func<DonacionMedicamentosDbContext, T> func)
        {
            T result;
            using (var context = LocalIocManager.Resolve<DonacionMedicamentosDbContext>())
            {
                context.DisableAllFilters();
                result = func(context);
                context.SaveChanges();
            }
            return result;
        }
    }
}
