﻿using System.Reflection;
using Abp.Application.Services;
using Abp.Configuration.Startup;
using Abp.Modules;
using Abp.WebApi;

namespace DonacionMedicamentos
{
    [DependsOn(typeof(AbpWebApiModule), typeof(DonacionMedicamentosApplicationModule))]
    public class DonacionMedicamentosWebApiModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(DonacionMedicamentosApplicationModule).Assembly, "app")
                .Build();
        }
    }
}
