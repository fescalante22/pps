﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;


namespace DonacionMedicamentos.Users
{
    /// <summary>
    /// Define un repositorio para mejorar las operaciones de la base de datos para las entidades de <see cref="User"/>
    /// Extiende de <see cref="IRepository{TEntity, TPrimaryKey}"/>  para herederar de las funcionalidades básicas del repositorio. 
    /// </summary>
    public interface IUsersRepository : IRepository<User>
    {
        /// <summary>
        /// Obtiene todos los usuarios
        /// </summary>
        /// <returns>Lista de todos los usuarios</returns>
        List<User> GetAllUsers();
    }

}
