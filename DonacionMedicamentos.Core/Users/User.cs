﻿using System;
using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace DonacionMedicamentos.Users
{
    /// <summary>
    /// Representa una entidad de Usuaro
    /// </summary>
    [Table("User")]
    public class User : Entity, ISoftDelete
    {
        /// <summary>
        /// Propiedad para guardar el numero de cuenta del usuario
        /// </summary>
        public virtual string Account { get; set; }

        /// <summary>
        /// Propiedad para el usuario para guardar su E- mail
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// Propiedad para el usuario para guardar su Password
        /// </summary>
        public virtual string Password { get; set; }

        /// <summary>
        /// Propiedad para el usuario para guardar su fecha de registro
        /// </summary>
        public virtual DateTime FechaRegistro { get; set; }

        /// <summary>
        /// Propiedad para saber si está eliminado o no
        /// ABP lo utiliza para borrar un elemento sin borrarlo de la base de datos
        /// </summary>
        public virtual bool IsDeleted { get; set; }

        /// <summary>
        /// Constructor de la clase User
        /// </summary>
        public User()
        {
            FechaRegistro = DateTime.Now;
        }
    }
}
