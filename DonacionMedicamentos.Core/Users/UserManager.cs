﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DonacionMedicamentos.MailingServices;
using Abp.Domain.Repositories;

namespace DonacionMedicamentos.Users
{
    public class UserManager : DomainService, IUserManager
    {
        //Instanciamos el repositorio de los usuarios
        private readonly IRepository<User> _userRepository;

        /// <summary>
        /// Constructor de UserManager
        /// </summary>
        public UserManager(IRepository<User> userRepository)
        {
            this._userRepository = userRepository;
        }

        /// <summary>
        /// Método para comprobar existencia por número de cuenta
        /// </summary>
        /// <param name="pUser">Usuario Nuevo</param>
        /// <returns>true si ya existe</returns>
        public bool ComprobarExistencia(User pUser)
        {
            bool valor = true;
            if (this.GetByAccount(pUser)== null)
            {
                valor = false;
            }
            return valor;
        }

        /// <summary>
        /// Método para cambiar Password
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public void ChangePass(User pUser,string pPasswordNueva)
        {
            User _user = this._userRepository.Get(pUser.Id);
            if (_user.Password == pUser.Password)
            {
                _user.Password = pPasswordNueva;
            }
            else
            {
                throw new Exception("La contraseña actual no es correcta");
            }

                
        }

        /// <summary>
        /// Método que comprueba si el usuario ingresado es correcto
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool Login(User pUser)
        {
            bool valor = false;
            User user = _userRepository.Single(x => x.Account == pUser.Account);
            if (user != null && (pUser.Password == user.Password))
            {
                valor = true;
            }
            else
            {
                throw new Exception("No se ha encontrado el usuario");
            }
            return valor;
        }

        /// <summary>
        /// Método para crear un usuario
        /// </summary>
        /// <param name="pUser"></param>
        public void Create(User pUser)
        {
            if(!this.ComprobarExistencia(pUser))
            {
                this._userRepository.Insert(pUser);
            }
            else
            {
                throw new Exception("El DNI ya se encuentra registrado.");
            }
            
        }

        /// <summary>
        /// Método para eliminar a un usuario (baja logica)
        /// </summary>
        /// <param name="pUser"></param>
        public void Delete(User pUser)
        {
            this._userRepository.Delete(pUser);
        }

        /// <summary>
        /// Metodo para obtener la informacion de un Usuario
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public User GetByAccount(User pUser)
        {
            return this._userRepository.FirstOrDefault(x => x.Account == pUser.Account);
        }

        public User Get(User pUser)
        {
            return this._userRepository.Get(pUser.Id);
        }
    }
}

