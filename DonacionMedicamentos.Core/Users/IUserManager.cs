﻿using Abp.Domain.Services;

namespace DonacionMedicamentos.Users
{
    /// <summary>
    /// Interfaz del Manager de la entidad Usuario
    /// </summary>
    public interface IUserManager : IDomainService
    {
        /// <summary>
        /// Método de interfaz para comprobar si existe este usuario
        /// </summary>
        /// <param name="pUser">Usuario Nuevo</param>
        /// <returns>bool</returns>
        bool ComprobarExistencia(User pUser);
        
        /// <summary>
        /// Método de interfaz para recuperar la Password de un usuario
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        //void RecoverPass(User pUser);

        /// <summary>
        /// Metodo de interfaz para crear un Usuario
        /// </summary>
        /// <param name="pUser"></param>
        void Create(User pUser);

        /// <summary>
        /// Metodo de interfaz para que un Usuario ingrese al sistema
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        bool Login(User pUser);

        /// <summary>
        /// Metodo de inerfaz para cambiar la contraseña del Usuario
        /// </summary>
        /// <param name="pUser"></param>
        /// <param name="pPasswordNueva"></param>
        void ChangePass(User pUser,string pPasswordNueva);

        /// <summary>
        /// Metodo de interfaz para realizar la baja logica del Usuario
        /// </summary>
        /// <param name="pUser"></param>
        void Delete(User pUser);

        /// <summary>
        /// Metodo de interfaz para obtener los datos de un Usuario a través de su cuenta
        /// </summary>
        User GetByAccount(User pUser);

        /// <summary>
        /// Metodo de interfaz para obtener los datos de un Usuario a través de su Id
        /// </summary>
        User Get(User pUser);
    }
}
