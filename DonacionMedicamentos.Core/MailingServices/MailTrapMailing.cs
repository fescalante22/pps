﻿using System.Net.Mail;
using System.Net;
using DonacionMedicamentos.MailingServices;

namespace DonacionMedicamentos.Mailing
{
    public class MailTrapMailing : IMailing
    {
        /// <summary>
        /// Envía un mail utilizando la API de MailTrap
        /// </summary>
        /// <param name="pTo"></param>
        /// <param name="pSubject"></param>
        /// <param name="pBody"></param>
        public void SendEmail(string pTo, string pSubject, string pBody)
        {            
            var client = new SmtpClient("smtp.mailtrap.io", 2525)
            {
                Credentials = new NetworkCredential("ac839870014149", "9d87aecdd65709"),
                EnableSsl = true
            };
            client.Send("9a59c138ff-16ebae@inbox.mailtrap.io", pTo, pSubject, pBody);
        } 
    }
}