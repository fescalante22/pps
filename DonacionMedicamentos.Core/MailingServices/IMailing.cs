﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.MailingServices
{
    /// <summary>
    /// Interface para establecer los metodos necesarios de servicios de mail
    /// </summary>
    public interface IMailing : IDomainService
    {
        /// <summary>
        /// Metodo de interfaz para enviar un Email
        /// </summary>
        /// <param name="pFrom"></param>
        /// <param name="pTo"></param>
        /// <param name="pSubject"></param>
        /// <param name="pBody"></param>
        void SendEmail(string pTo, string pSubject, string pBody);
    }
}
