﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Drugs
{
    /// <summary>
    /// Representa una entidad de Medicamento
    /// </summary>
    [Table("Drug")]
    public class Drug : Entity
    {
        /// <summary>
        /// Propiedad del medicamento para indicar el nombre generico
        /// </summary>
        public virtual string NombreGenerico { get; set; }

        /// <summary>
        /// Propiedad del medicamento para indicar el nombre comercial
        /// </summary>
        public virtual string NombreComercial { get; set; }

        /// <summary>
        /// Propiedad del medicamento para indicar la dosis
        /// </summary>
        public virtual string Dosis  { get; set; }

        /// <summary>
        /// Constructor de la clase Drug
        /// </summary>
        public Drug() { }
    }
}
