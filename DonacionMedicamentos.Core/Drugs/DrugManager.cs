﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Drugs
{
    public class DrugManager : IDrugManager
    {
        private readonly IRepository<Drug> _drugRepository;


        public DrugManager(IRepository<Drug> drugRepository)
        {
            _drugRepository = drugRepository;
        }

        public Drug Get(Drug pDrug)
        {
            return this._drugRepository.Get(pDrug.Id);
        }

        /// <summary>
        /// Obtener todos los medicamentos
        /// </summary>
        /// <returns></returns>
        public List<Drug> GetAll()
        {
            return this._drugRepository.GetAllList();
        }
    }
}
