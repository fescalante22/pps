﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Drugs
{
    public interface IDrugManager : IDomainService
    {
        List<Drug> GetAll();

        Drug Get(Drug pDrug);
    }
}
