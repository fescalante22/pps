﻿using Abp.Domain.Entities;
using DonacionMedicamentos.Comments;
using DonacionMedicamentos.DrugsRequests;
using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Personas;
using DonacionMedicamentos.RequestStates;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Requests
{
    /// <summary>
    /// Representa una entidad de Solicitud
    /// </summary>
    [Table("Request")]
    public class Request : Entity
    {
        /// <summary>
        /// Propiedad para guardar la fecha de creacion de la solicitud
        /// </summary>
        public virtual DateTime Fecha { get; set; }

        /// <summary>
        /// Propiedad para guardar la direccion de almacenamiento de la imagen de la receta
        /// </summary>
        public virtual string RutaReceta { get; set; }

        /// <summary>
        /// Propiedad para guardar los medicamentos solicitados
        /// </summary>
        public virtual List<DrugsRequest> Medicamentos { get; set; }

        /// <summary>
        /// Propiedad para guardar, en el caso que existieran, las observaciones registradas por los intermediarios
        /// </summary>
        public virtual List<Comment> Observaciones { get; set; }

        /// <summary>
        /// Propiedad para guardar el estado en el que se encuentra la solicitud
        /// </summary>
        [EnumDataType(typeof(RequestState))]
        public virtual RequestState Estado { get; set; }

        /// <summary>
        /// Propiedad para guardar la persona que registro la solicitud
        /// </summary>
        public virtual Person Persona { get; set; }

        /// <summary>
        /// Propiedad para guardar el intermediario cuando aprueba la solicitud
        /// </summary>
        public virtual Intermediary Intermediario { get; set; }

        /// <summary>
        /// Constructor de la clase Request
        /// </summary>
        public Request()
        {
            this.Fecha = DateTime.Now;
            this.Estado = RequestState.Registrada;
        }
    }
}
