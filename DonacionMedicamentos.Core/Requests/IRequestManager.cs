﻿using Abp.Domain.Services;
using DonacionMedicamentos.Comments;
using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Personas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Requests
{
    /// <summary>
    /// Interfaz de la clase manager de la solicitud
    /// </summary>
    public interface IRequestManager : IDomainService
    {
        /// <summary>
        /// Metodo de interfaz para crear una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        void RegisterRequest(Request pRequest);

        /// <summary>
        /// Metodo de interfaz para modificar una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        void UpdateRequest(Request pRequest);

        /// <summary>
        /// Metodo de interfaz para cancelar una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        void CancelRequest(Request pRequest);

        /// <summary>
        /// Metodo de interfaz para que un intermediario apruebe una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        void Approve(Request pRequest, Intermediary pIntermediary);

        /// <summary>
        /// Metodo de interfaz para eliminar la relacion entre solicitud e Intermediario
        /// Y cambiar la solicitud al estado correspondiente
        /// </summary>
        void Disassociate(Request pRequest);

        /// <summary>
        /// Metodo de interfaz para agregar un comentario a una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        /// <param name="pComment"></param>
        void AddComment(Request pRequest, Comment pComment);

        /// <summary>
        /// Metodo de interfaz para obtener una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        /// <returns></returns>
        Request Get(Request pRequest);

        /// <summary>
        /// Metodo de interfaz para obtener una solicitud por parte de un intermediario
        /// </summary>
        /// <param name="pRequest"></param>
        /// <returns></returns>
        Request Get(Request pRequest,Intermediary pIntermediary);

        /// <summary>
        /// Metodo de interfaz para obtener una solicitud por parte de una persona
        /// </summary>
        /// <param name="pRequest"></param>
        /// <returns></returns>
        Request Get(Request pRequest, Person pPerson);

        /// <summary>
        /// Metodo de interfaz para obtener todas las solicitudes
        /// </summary>
        /// <returns></returns>
        List<Request> GetRequests();

        /// <summary>
        /// Metodo de interfaz para obtener todas las solicitudes aprobadas por un intermediario
        /// </summary>
        /// <param name="pIntermediary"></param>
        /// <returns></returns>
        List<Request> GetRequests(Intermediary pIntermediary);

        /// <summary>
        /// Metodo de interfaz para obtener todas las solicitudes registradas por una persona
        /// </summary>
        /// <param name="pPerson"></param>
        /// <returns></returns>
        List<Request> GetRequests(Person pPerson);


        /// <summary>
        /// Método de interfaz para cambiar el estado de una solicitud
        /// </summary>
        /// <param name="input"></param>
        void ChangeRequestState(Request input);


    }
}
