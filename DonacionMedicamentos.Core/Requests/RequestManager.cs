﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DonacionMedicamentos.Comments;
using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Personas;
using Abp.Domain.Repositories;
using DonacionMedicamentos.RequestStates;

namespace DonacionMedicamentos.Requests
{
    /// <summary>
    /// Clase manager de la entidad Request
    /// </summary>
    public class RequestManager : IRequestManager
    {
        //Instanciamos el respositorio de las solicitudes
        private readonly IRepository<Request> _requestRepository;

        /// <summary>
        /// Constructor de la clase manager de Request
        /// </summary>
        /// <param name="requestRepository"></param>
        public RequestManager(
            IRepository<Request> requestRepository)
        {
            this._requestRepository = requestRepository;
        }

        /// <summary>
        /// Metodo para agregar un comentario a una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        /// <param name="pComment"></param>
        public void AddComment(Request pRequest, Comment pComment)
        {
            pRequest.Observaciones.Add(pComment);
            this._requestRepository.Update(pRequest);
        }

        /// <summary>
        /// Metodo para que un intermediario apruebe una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        public void Approve(Request pRequest, Intermediary pIntermediary)
        {
            if (pRequest.Estado == RequestState.Registrada)
            {
                pRequest.Estado = RequestState.Aprobada;
                pRequest.Intermediario = pIntermediary;
            }
        }

        /// <summary>
        /// Metodo para eliminar la relacion entre solicitud e Intermediario
        /// Y cambiar la solicitud al estado correspondiente
        /// </summary>
        public void Disassociate(Request pRequest)
        {
            Request request = this.Get(pRequest);
            request.Intermediario = null;
            if (request.Estado == RequestState.Aprobada)
            {
                request.Estado = RequestState.Registrada;
            }
            else if (request.Estado != RequestState.Entregada)
            {
                request.Estado = RequestState.Cancelada;
            }
        }

        /// <summary>
        /// Metodo para cancelar una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        public void CancelRequest(Request pRequest)
        {
            pRequest.Estado = RequestState.Cancelada;
        }

        public void ChangeRequestState(Request pRequest)
        {
            Dictionary<int, List<int>> controlStates = new Dictionary<int, List<int>>();

            //From Registrada
            controlStates.Add((int)RequestState.Registrada, new List<int>{
                (int)RequestState.Cancelada
            });

            // From Aprobada
            controlStates.Add((int)RequestState.Aprobada, new List<int> {
                (int)RequestState.EnDistribucion,
                (int)RequestState.Entregada,
                (int)RequestState.Cancelada
            });

            // From En Distribución
            controlStates.Add((int)RequestState.EnDistribucion, new List<int> {
                (int)RequestState.Entregada,
                (int)RequestState.EnDistribucion,
                (int)RequestState.Cancelada
            });

            // From Entregada
            controlStates.Add((int)RequestState.Entregada, new List<int> {
                (int)RequestState.Entregada
            });

            // From Cancelada
            controlStates.Add((int)RequestState.Cancelada, new List<int> {
                (int)RequestState.Cancelada
            });

            Request oldRequest = this._requestRepository.Get(pRequest.Id);
            int oldState = (int)oldRequest.Estado;
            int newState = (int)pRequest.Estado;

            if(controlStates[oldState].Exists(x => x == newState))
            {
                oldRequest.Estado = pRequest.Estado;
                this.UpdateRequest(oldRequest);
            }
            else
            {
                throw new Exception("El pasaje de estados no es válido");
            }
        }

        /// <summary>
        /// Metodo para obtener una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        /// <returns></returns>
        public Request Get(Request pRequest)
        {
            return this._requestRepository.Get(pRequest.Id);
        }

        /// <summary>
        /// Metodo para obtener una solicitud por parte de un intermediario, controlando si el intermediario puede acceder a la misma
        /// </summary>
        /// <param name="pRequest"></param>
        /// <param name="pIntermediary"></param>
        /// <returns></returns>
        public Request Get(Request pRequest, Intermediary pIntermediary)
        {
            Request request = this._requestRepository.Get(pRequest.Id);
            if ((request.Estado != RequestState.Cancelada) && (request.Intermediario == pIntermediary || request.Intermediario == null))
            {
                return request;
            }
            throw new Exception("No se encontró la solicitud");
        }

        /// <summary>
        /// Metodo para obtener una solicitud por parte de una persona, controlando si la persona puede acceder a la misma
        /// </summary>
        /// <param name="pRequest"></param>
        /// <param name="pPerson"></param>
        /// <returns></returns>
        public Request Get(Request pRequest, Person pPerson)
        {
            Request request = this._requestRepository.Get(pRequest.Id);
            if (request.Persona.Id == pPerson.Id)
            {
                return request;
            }
            throw new Exception("No se encontró la solicitud");
        }

        /// <summary>
        /// Metodo para obtener todas las solicitudes
        /// </summary>
        /// <returns></returns>
        public List<Request> GetRequests()
        {
            return this._requestRepository.GetAllList();
        }

        /// <summary>
        /// Metodo para obtener todas las solicitudes registradas por una persona
        /// </summary>
        /// <param name="pPerson"></param>
        /// <returns></returns>
        public List<Request> GetRequests(Person pPerson)
        {
            return this._requestRepository.GetAllList(x => x.Persona.Id == pPerson.Id);
        }

        /// <summary>
        /// Metodo para obtener todas las solicitudes aprobadas por un intermediario
        /// </summary>
        /// <param name="pIntermediary"></param>
        /// <returns></returns>
        public List<Request> GetRequests(Intermediary pIntermediary)
        {
            return this._requestRepository.GetAllList().FindAll(x => (x.Intermediario != null) && (x.Intermediario.Id == pIntermediary.Id) && (x.Estado != RequestState.Cancelada));
        }

        /// <summary>
        /// Metodo para crear una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        public void RegisterRequest(Request pRequest)
        {
           this._requestRepository.Insert(pRequest);
        }

        /// <summary>
        /// Metodo para modificar una solicitud
        /// </summary>
        /// <param name="pRequest"></param>
        public void UpdateRequest(Request pRequest)
        {
            this._requestRepository.Update(pRequest);
        }
    }
}
