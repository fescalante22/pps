﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Comments
{
    public interface ICommentManager : IDomainService
    {
        void Create(Comment pComment);
    }
}
