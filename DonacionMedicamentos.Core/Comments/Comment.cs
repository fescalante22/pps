﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Comments
{
    /// <summary>
    /// Representa una entidad de Observacion
    /// </summary>
    [Table("Comment")]
    public class Comment : Entity
    {
        /// <summary>
        /// Propiedad de la observacion para guardar su fecha de creacion
        /// </summary>
        public virtual DateTime Fecha { get; set; }

        /// <summary>
        /// Propiedad de la observacion para guardar su detalle
        /// </summary>
        public virtual string Detalle { get; set; }

        /// <summary>
        /// Constructor de la clase Comment
        /// </summary>
        public Comment()
        {
            this.Fecha = DateTime.Now;
        }
    }
}
