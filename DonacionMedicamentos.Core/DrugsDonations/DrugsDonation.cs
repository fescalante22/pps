﻿using Abp.Domain.Entities;
using DonacionMedicamentos.Drugs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.DrugsDonations
{
    /// <summary>
    /// Representa la relacion entre un Medicamento y una Donacion, añadiendo cantidad y fecha de vencimiento
    /// </summary>
    [Table("DrugsDonation")]
    public class DrugsDonation : Entity
    {
        /// <summary>
        /// Propiedad para indicar la cantidad del medicamento
        /// </summary>
        public virtual int Cantidad { get; set; }

        /// <summary>
        /// Propiedad para indicar el medicamento al que se hace referencia
        /// </summary>
        public virtual Drug Medicamento { get; set; }

        /// <summary>
        /// Propiedad para indicar la fecha de vencimiento del medicamento
        /// </summary>
        public virtual DateTime FechaVencimiento { get; set; }

        /// <summary>
        /// Constructor de la clase DrugsDonation
        /// </summary>
        public DrugsDonation() { }
    }
}
