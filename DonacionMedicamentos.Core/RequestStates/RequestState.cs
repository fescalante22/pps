﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace DonacionMedicamentos.RequestStates
{
    public enum RequestState
    {
        Registrada = 0,
        Aprobada = 1,
        EnDistribucion = 2,        
        Entregada = 3,
        Cancelada = 4
    }
}
