﻿using Abp.Domain.Entities;
using DonacionMedicamentos.Drugs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.DrugsRequests
{
    /// <summary>
    /// Representa una entidad de Medicamento de una Solicitud
    /// </summary>
    [Table("DrugsRequest")]
    public class DrugsRequest : Entity
    {
        /// <summary>
        /// Propiedad para indicar la cantidad del medicamento
        /// </summary>
        public virtual int Cantidad { get; set; }

        /// <summary>
        /// Propiedad para indicar el medicamento al que se hace referencia
        /// </summary>
        public virtual Drug Medicamento { get; set; }

        /// <summary>
        /// Constructor de la clase DrugsRequest
        /// </summary>
        public DrugsRequest() { }
    }
}
