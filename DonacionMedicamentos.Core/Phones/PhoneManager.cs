﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Phones
{
    /// <summary>
    /// Clase para administrar la logica relacionada a Phone
    /// </summary>
    public class PhoneManager : IPhoneManager
    {
        private readonly IRepository<Phone> _phoneRepository;

        /// <summary>
        /// Constructor de la clase Phone
        /// </summary>
        /// <param name="phoneRepository"></param>
        public PhoneManager(IRepository<Phone> phoneRepository)
        {
            _phoneRepository = phoneRepository;
        }

        /// <summary>
        /// Metodo para registrar un telefono
        /// </summary>
        /// <param name="pPhone"></param>
        public void Create(Phone pPhone)
        {
            this._phoneRepository.Insert(pPhone);
        }

        /// <summary>
        /// Metodo para modificar un telefono
        /// </summary>
        /// <param name="pPhone"></param>
        public void Update(Phone pPhone)
        {
            this._phoneRepository.Update(pPhone);
        }
        
        /// <summary>
        /// Metodo para eliminar un telefono
        /// </summary>
        /// <param name="pPhone"></param>
        public void Delete(Phone pPhone)
        {
            this._phoneRepository.Delete(pPhone);
        }
    }
}
