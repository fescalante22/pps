﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Phones
{
    /// <summary>
    /// Interfaz para definir el comportamiento de la entidad Phone
    /// </summary>
    public interface IPhoneManager : IDomainService
    {
        /// <summary>
        /// Metodo de interfaz para registrar un telefono
        /// </summary>
        /// <param name="pPhone"></param>
        void Create(Phone pPhone);

        /// <summary>
        /// Metodo de interfaz para modificar un telefono
        /// </summary>
        /// <param name="pPhone"></param>
        void Update(Phone pPhone);

        /// <summary>
        /// Metodo de interfaz para eliminar un telefono
        /// </summary>
        /// <param name="pPhone"></param>
        void Delete(Phone pPhone);
    }
}
