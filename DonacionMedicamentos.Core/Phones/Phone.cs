﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Phones
{
    /// <summary>
    /// Representa una entidad de Telefono
    /// </summary>
    [Table("Phone")]
    public class Phone : Entity
    {
        /// <summary>
        /// Propiedad del telefono para guardar su ccodigo de area
        /// </summary>
        public virtual int CodigoArea { get; set; }
       
        /// <summary>
        /// Propiedad del telefono para guardar su numero
        /// </summary>
        public virtual int Numero { get; set; }

        /// <summary>
        /// Constructor de la clase Phone
        /// </summary>
        public Phone() { }
    }
}
