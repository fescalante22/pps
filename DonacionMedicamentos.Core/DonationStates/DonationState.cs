﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.DonationStates
{
    public enum DonationState
    {
        Registrada = 0,
        Aprobada = 1,
        EnRetiro = 2,
        Almacenada = 3,
        EntregadaParcialmente = 4,
        Entregada = 5,
        Cancelada = 6
    }
}
