﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Intermediarios
{
    public interface IIntermediaryManager : IDomainService
    {
        /// <summary>
        /// Metodo de interfaz para crear un intermediario
        /// </summary>
        /// <param name="pIntermediary"></param>
        void Create(Intermediary pIntermediary);

        /// <summary>
        /// Metodo de interfaz para modificar un Intermediario
        /// </summary>
        /// <param name="pIntermediary"></param>
        void Update(Intermediary pIntermediary);

        /// <summary>
        /// Metodo de interfaz para obtener un Intermediario
        /// </summary>
        /// <param name="pIntermediary"></param>
        /// <returns></returns>
        Intermediary Get(Intermediary pIntermediary);


        /// <summary>
        /// Metodo de interfaz para obtener un Intermediario a través de su número de cuenta
        /// </summary>
        /// <param name="pIntermediary"></param>
        /// <returns></returns>
        Intermediary GetByAccount(Intermediary pIntermediary);

        /// <summary>
        /// Metodo de interfaz para obtener todos los intermediarios
        /// </summary>
        /// <returns></returns>
        List<Intermediary> GetAll();

        /// <summary>
        /// Metodo de interfaz para realizar la baja logica de un intermediario
        /// </summary>
        /// <param name="pIntermediary"></param>
        void Delete(Intermediary pIntermediary);
    }

}
