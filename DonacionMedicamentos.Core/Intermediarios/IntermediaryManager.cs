﻿using Abp.Domain.Repositories;
using DonacionMedicamentos.Phones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Intermediarios
{
    public class IntermediaryManager : IIntermediaryManager
    {
        private readonly IRepository<Intermediary> _intermediaryRepository;
        private readonly IRepository<Phone> _phoneRepository;

        /// <summary>
        /// Constructor del manager del intermediario
        /// </summary>
        /// <param name="intermediarioRepository"></param>
        public IntermediaryManager(IRepository<Intermediary> intermediaryRepository, IRepository<Phone> phoneRepository)
        {
            this._intermediaryRepository = intermediaryRepository;
            this._phoneRepository = phoneRepository;
        }

        /// <summary>
        /// Insertar un intermediario en la base de datos
        /// </summary>
        /// <param name="pIntermediary"></param>
        public void Create(Intermediary pIntermediary)
        {
            //Pregunto si existe el DNI
            Intermediary sameCUIL = this._intermediaryRepository.FirstOrDefault(x => x.Account == pIntermediary.Account);
            if (sameCUIL == null)
            {
                // Pregunto si existe el email
                Intermediary sameMail = this._intermediaryRepository.FirstOrDefault(x => x.Email == pIntermediary.Email);
                if (sameMail == null)
                {
                    this._intermediaryRepository.Insert(pIntermediary);
                }
                else
                {
                    throw new Exception("El Email ya se encuentra registrado.");
                }
            }
            else
            {
                throw new Exception("El CUIT/CUIL ya se encuentra registrado.");
            }
        }

        /// <summary>
        /// Metodo para realizar la baja logica de un intermediario
        /// </summary>
        /// <param name="pIntermediary"></param>
        public void Delete(Intermediary pIntermediary)
        {
            this._intermediaryRepository.Delete(pIntermediary);
        }

        /// <summary>
        /// Obtener un intermediario por número de cuenta
        /// </summary>
        /// <param name="pIntermediary"></param>
        /// <returns></returns>
        public Intermediary Get(Intermediary pIntermediary)
        {
            return this._intermediaryRepository.Get(pIntermediary.Id);
        }

        /// <summary>
        /// Metodo para obtener todos los intermediarios
        /// </summary>
        /// <returns></returns>
        public List<Intermediary> GetAll()
        {
            return this._intermediaryRepository.GetAllList();
        }

        /// <summary>
        /// Metodo para obtener un intermediario por su numero de cuenta
        /// </summary>
        /// <param name="pIntermediary"></param>
        /// <returns></returns>
        public Intermediary GetByAccount(Intermediary pIntermediary)
        {
            return this._intermediaryRepository.Single(x => x.Account == pIntermediary.Account);
        }

        /// <summary>
        /// Metodo para actualizar un intermediario
        /// </summary>
        /// <param name="pIntermediary"></param>
        public void Update(Intermediary pIntermediary)
        {
            /*try
            {
                this._intermediaryRepository.Single(x => x.Account == pIntermediary.Account);
                throw new Exception("El CUIT o CUIL ya se encuentra registrado");
            }
            catch (InvalidOperationException ex)
            {
                this._intermediaryRepository.Update(pIntermediary);
            }*/
            this._intermediaryRepository.Update(pIntermediary);

        }
    }
}
