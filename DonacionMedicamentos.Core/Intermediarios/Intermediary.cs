﻿using Abp.Domain.Entities;
using DonacionMedicamentos.Cities;
using DonacionMedicamentos.Phones;
using DonacionMedicamentos.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Intermediarios
{
    /// <summary>
    /// Representa una entidad de un Intermediario
    /// </summary>
    [Table("Intermediary")]
    public class Intermediary : User
    {
        /// <summary>
        /// Propiedad para el Intermediario para guardar su Nombre
        /// </summary>
        public virtual string Nombre { get; set; }

        /// <summary>
        /// Propiedad para el Intermediario para guardar su calle
        /// </summary>
        public virtual string Calle { get; set; }

        /// <summary>
        /// Propiedad para el Intermediario para guardar la altura de la calle
        /// </summary>
        public virtual int Numero { get; set; }

        /// <summary>
        /// Propiedad para el Intermediario para guardar el piso de su domicilio, si tuviera.
        /// </summary>
        public virtual int Piso { get; set; }

        /// <summary>
        /// Propiedad para el Intermediario para guardar el numero de departamento de su domicilio, si tuviera.
        /// </summary>
        public virtual int Departamento { get; set; }

        /// <summary>
        /// Propiedad para el Intermediario para guardar la ciudad donde recide
        /// </summary>
        public virtual City Ciudad { get; set; }

        /// <summary>
        /// Propiedad para el Intermediario para guardar sus telefonos
        /// </summary>
        public virtual List<Phone> Telefonos { get; set; }

        /// <summary>
        /// Constructor de la clase de Intermediario
        /// </summary>
        public Intermediary() : base()
        {
            //Inicializamos las listas del Intermediario
            this.Telefonos = new List<Phone>();
        }

    }
}
