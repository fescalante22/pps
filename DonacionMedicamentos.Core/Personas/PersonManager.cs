﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DonacionMedicamentos.Donations;
using DonacionMedicamentos.Requests;
using Abp.Domain.Repositories;
using DonacionMedicamentos.Users;

namespace DonacionMedicamentos.Personas
{
    /// <summary>
    /// Clase manager de la entidad Persona
    /// </summary>
    public class PersonManager : IPersonManager
    {
        private readonly IRepository<Person> _personRepository;

        /// <summary>
        /// Constructor del manager de la persona
        /// </summary>
        /// <param name="personRepository"></param>
        public PersonManager(
            IRepository<Person> personRepository)
        {
            this._personRepository = personRepository;
        }

        /// <summary>
        /// Metodo para crear una Persona
        /// </summary>
        /// <param name="pPerson"></param>
        public void Create(Person pPerson)
        {
            //Pregunto si existe el DNI
            Person sameDNI = this._personRepository.FirstOrDefault(x => x.Account == pPerson.Account);
            if (sameDNI == null)
            { 
                // Pregunto si existe el email
                Person sameMail = this._personRepository.FirstOrDefault(x => x.Email == pPerson.Email);
                if (sameMail == null)
                {
                    this._personRepository.Insert(pPerson);
                }
                else
                {
                    throw new Exception("El Email ya se encuentra registrado.");
                }
            }
            else
            {
                throw new Exception("El DNI ya se encuentra registrado.");
            }

        }

        /// <summary>
        /// Metodo para modificar una persona
        /// </summary>
        /// <param name="pPerson"></param>
        public void Update(Person pPerson)
        {
            /* try
             {
                 this._personRepository.Single(x => x.Account == pPerson.Account);
                 throw new Exception("El DNI ya se encuentra registrado");
             }
             catch (InvalidOperationException ex)
             {
                 this._personRepository.Update(pPerson);
             }      */
            this._personRepository.Update(pPerson);
        }

        /// <summary>
        /// Metodo para obtener una persona
        /// </summary>
        /// <param name="pPerson"></param>
        /// <returns></returns>
        public Person Get(Person pPerson)
        {
            return this._personRepository.Get(pPerson.Id);
        }

        /// <summary>
        /// Metodo para obtener una persona a partir del numero de cuenta (account)
        /// </summary>
        /// <param name="pPerson"></param>
        /// <returns></returns>
        public Person GetByAccount(Person pPerson)
        {
            return this._personRepository.Single(x => x.Account == pPerson.Account);
        }
    }
}
