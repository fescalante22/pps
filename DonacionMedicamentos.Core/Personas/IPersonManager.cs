﻿using Abp.Domain.Services;
using DonacionMedicamentos.Donations;
using DonacionMedicamentos.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Personas
{
    public interface IPersonManager : IDomainService
    {
        /// <summary>
        /// Metodo de interfaz para crear una Persona
        /// </summary>
        /// <param name="pPerson"></param>
        void Create(Person pPerson);

        /// <summary>
        /// Metodo de interfaz para modificar una Persona
        /// </summary>
        /// <param name="pPerson"></param>
        void Update(Person pPerson);

        /// <summary>
        /// Metodo de interfaz para obtener los datos de una persona
        /// </summary>
        Person Get(Person pPerson);

        /// <summary>
        /// Metodo de interfaz para obtener los datos de una persona
        /// </summary>
        Person GetByAccount(Person pPerson);
        
    }
}
