﻿using Abp.Domain.Entities;
using DonacionMedicamentos.Cities;
using DonacionMedicamentos.Donations;
using DonacionMedicamentos.Phones;
using DonacionMedicamentos.Requests;
using DonacionMedicamentos.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Personas
{
    /// <summary>
    /// Representa una entidad de Persona
    /// </summary>
    [Table("Person")]
    public class Person : User
    {
        /// <summary>
        /// Propiedad para la persona para guardar su Nombre
        /// </summary>
        public virtual string Nombre { get; set; }

        /// <summary>
        /// Propiedad para la persona para guardar su Apellido
        /// </summary>
        public virtual string Apellido { get; set; }

        /// <summary>
        /// Propiedad para la persona para guardar su Genero
        /// </summary>
        public virtual string Genero { get; set; }

        /// <summary>
        /// Propiedad para la persona para guardar su calle
        /// </summary>
        public virtual string Calle { get; set; }

        /// <summary>
        /// Propiedad para la persona para guardar la altura de la calle
        /// </summary>
        public virtual int Numero { get; set; }

        /// <summary>
        /// Propiedad para la persona para guardar el piso de su domicilio, si tuviera.
        /// </summary>
        public virtual int Piso { get; set; }

        /// <summary>
        /// Propiedad para la persona para guardar el numero de departamento de su domicilio, si tuviera.
        /// </summary>
        public virtual int Departamento { get; set; }

        /// <summary>
        /// Propiedad para la persona para guardar la fecha de nacimiento
        /// </summary>
        public virtual DateTime FechaNacimiento { get; set; }

        /// <summary>
        /// Propiedad para la persona para guardar la ciudad donde recide
        /// </summary>
        public virtual City Ciudad { get; set; }
        
        /// <summary>
        /// Propiedad para la persona para guardar sus telefonos
        /// </summary>
        public virtual List<Phone> Telefonos { get; set; }

        /// <summary>
        /// Constructor de la clase Person
        /// </summary>
        public Person() : base()
        {
            this.FechaRegistro = DateTime.Today;
            //Inicializamos las listas de la persona
            this.Telefonos = new List<Phone>();
        }
    }
}
