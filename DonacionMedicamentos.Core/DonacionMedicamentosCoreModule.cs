﻿using System.Reflection;
using Abp.Modules;
using DonacionMedicamentos.Authorization;

namespace DonacionMedicamentos
{
    public class DonacionMedicamentosCoreModule : AbpModule
    {

        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<DonacionMedicamentosAuthorizationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
