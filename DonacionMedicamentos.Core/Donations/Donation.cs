﻿using Abp.Domain.Entities;
using DonacionMedicamentos.DonationStates;
using DonacionMedicamentos.DrugsDonations;
using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Personas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Donations
{
    /// <summary>
    /// Representa una entidad de Donacion
    /// </summary>
    [Table("Donation")]
    public class Donation : Entity
    {
        /// <summary>
        /// Propiedad de la donacion para guardar la fecha de creacion
        /// </summary>
        public virtual DateTime Fecha { get; set; }

        /// <summary>
        /// Propiedad de la donacion para guardar los medicamentos asociados a la misma
        /// </summary>
        public virtual List<DrugsDonation> Medicamentos { get; set; }

        /// <summary>
        /// Propiedad de la donacion para guardar el estado actual
        /// </summary>
        [EnumDataType(typeof(DonationState))]
        public virtual DonationState Estado { get; set; }

        /// <summary>
        /// Propiedad de la donacion para guardar la persona que la registro
        /// </summary>
        public virtual Person Persona { get; set; }

        /// <summary>
        /// Propiedad de la donacion para guardar el intermediario que la aprobó, en caso que sucediera
        /// </summary>
        public virtual Intermediary Intermediario { get; set; }

        /// <summary>
        /// Constructor de la clase Donation
        /// </summary>
        public Donation()
        {
            this.Fecha = DateTime.Now;
            this.Estado = DonationState.Registrada;
        }
    }
}
