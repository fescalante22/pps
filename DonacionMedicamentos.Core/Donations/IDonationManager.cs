﻿using Abp.Domain.Services;
using DonacionMedicamentos.Intermediarios;
using DonacionMedicamentos.Personas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Donations
{
    /// <summary>
    /// Interfaz del manager de la entidad donacion
    /// </summary>
    public interface IDonationManager : IDomainService
    {
        /// <summary>
        /// Metodo de interfaz para registrar una donacion 
        /// </summary>
        /// <param name="pDonation"></param>
        void RegisterDonation(Donation pDonation);

        /// <summary>
        /// Metodo de interfaz para modificar una donacion
        /// </summary>
        /// <param name="pDonation"></param>
        void UpdateDonation(Donation pDonation);

        /// <summary>
        /// Metodo de interfaz para cancelar una donacion
        /// </summary>
        /// <param name="pDonation"></param>
        void CancelDonation(Donation pDonation);

        /// <summary>
        /// Metodo de interfaz para aprobar una donacion
        /// </summary>
        /// <param name="pDonation"></param>
        /// <param name="pIntermediary"></param>
        void Approve(Donation pDonation, Intermediary pIntermediary);

        /// <summary>
        /// Metodo de interfaz para eliminar la relacion entre donacion e Intermediario
        /// Y cambiar la donacion al estado correspondiente
        /// </summary>
        void Disassociate(Donation pDonation);

        /// <summary>
        /// Metodo de interfaz para obtener una donacion
        /// </summary>
        /// <param name="pDonation"></param>
        /// <returns></returns>
        Donation Get(Donation pDonation);

        /// <summary>
        /// Metodo de interfaz para obtener una donación por parte de un intermediario
        /// </summary>
        /// <param name="pDonation"></param>
        /// <returns></returns>
        Donation Get(Donation pDonation, Intermediary pIntermediary);

        /// <summary>
        /// Metodo de interfaz para obtener una donación por parte de una persona
        /// </summary>
        /// <param name="pDonation"></param>
        /// <returns></returns>
        Donation Get(Donation pDonation, Person pPerson);

        /// <summary>
        /// Metodo de interfaz para obtener todas las donaciones
        /// </summary>
        /// <returns></returns>
        List<Donation> GetDonations();

        /// <summary>
        /// Metodo de interfaz para obtener todas las donaciones aprobadas por un intermediario
        /// </summary>
        /// <param name="pIntermediary"></param>
        /// <returns></returns>
        List<Donation> GetDonations(Intermediary pIntermediary);

        /// <summary>
        /// Metodo de interfaz para obtener todas las donaciones registradas por una persona
        /// </summary>
        /// <param name="pPerson"></param>
        /// <returns></returns>
        List<Donation> GetDonations(Person pPerson);


        /// <summary>
        /// Método de interfaz para cambiar el estado de una donación
        /// </summary>
        /// <param name="pDonation"></param>
        void ChangeDonationState(Donation pDonation);
    }
}
