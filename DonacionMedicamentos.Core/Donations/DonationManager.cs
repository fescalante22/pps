﻿using DonacionMedicamentos.Personas;
using System;
using System.Collections.Generic;
using DonacionMedicamentos.Intermediarios;
using Abp.Domain.Repositories;
using DonacionMedicamentos.DonationStates;
using System.Linq;
using DonacionMedicamentos.RequestStates;

namespace DonacionMedicamentos.Donations
{
    /// <summary>
    /// Clase manager de la entidad Donacion
    /// </summary>
    public class DonationManager : IDonationManager
    {
        //Instanciamos el repositorio de las donaciones
        private readonly IRepository<Donation> _donationRepository;

        /// <summary>
        /// Constructor de la clase DonationManager
        /// </summary>
        /// <param name="donationRepository"></param>
        public DonationManager(
            IRepository<Donation> donationRepository)
        {
            this._donationRepository = donationRepository;
        }

        /// <summary>
        /// Metodo para que un intermediario apruebe una donacion
        /// </summary>
        /// <param name="pDonation"></param>
        /// <param name="pIntermediary"></param>
        public void Approve(Donation pDonation, Intermediary pIntermediary)
        {
            if (pDonation.Estado == DonationState.Registrada)
            {
                pDonation.Estado = DonationState.Aprobada;
                pDonation.Intermediario = pIntermediary;
            }
        }

        /// <summary>
        /// Metodo para eliminar la relacion entre donacion e Intermediario
        /// Y cambiar la donacion al estado correspondiente
        /// </summary>
        public void Disassociate(Donation pDonation)
        {
            Donation donation = this.Get(pDonation);
            donation.Intermediario = null;
            if (donation.Estado == DonationState.Aprobada)
            {
                donation.Estado = DonationState.Registrada;
            }
            else if (donation.Estado != DonationState.Entregada)
            {
                donation.Estado = DonationState.Cancelada;
            }
        }

        /// <summary>
        /// Metodo para cancelar una donacion
        /// </summary>
        /// <param name="pDonation"></param>
        public void CancelDonation(Donation pDonation)
        {
            pDonation.Estado = DonationState.Cancelada;
        }

        /// <summary>
        /// Método para cambiar el estado de la donación
        /// </summary>
        /// <param name="pDonation"></param>
        public void ChangeDonationState(Donation pDonation)
        {
            Dictionary<int, List<int>> controlStates = new Dictionary<int, List<int>>();

            //From Registrada
            controlStates.Add((int)DonationState.Registrada,new List<int> {
                (int)DonationState.Cancelada
            });

            // From Aprobada
            controlStates.Add((int)DonationState.Aprobada,new List<int> {
                (int)DonationState.Almacenada,
                (int)DonationState.EnRetiro,
                (int)DonationState.Cancelada
            });

            // From En Retiro
            controlStates.Add((int)DonationState.EnRetiro, new List<int> {
                (int)DonationState.Almacenada,
                (int)DonationState.EnRetiro,
                (int)DonationState.Cancelada,
            });

            // From Almacenada
            controlStates.Add((int)DonationState.Almacenada, new List<int> {
                (int)DonationState.EntregadaParcialmente,
                (int)DonationState.Entregada,
                (int)DonationState.Almacenada,
                (int)DonationState.Cancelada,
            });

            // From EntregadaParcial
            controlStates.Add((int)DonationState.EntregadaParcialmente, new List<int>
            {
                (int)DonationState.Entregada,
                (int)DonationState.EntregadaParcialmente,
                (int)DonationState.Cancelada,
            });

            // From Entregada
            controlStates.Add((int)DonationState.Entregada, new List<int>
            {
                (int)DonationState.Entregada
            });
            
            // From Cancelada
            controlStates.Add((int)DonationState.Cancelada, new List<int>
            {
                (int)DonationState.Cancelada
            });

            Donation oldDonation = this._donationRepository.Get(pDonation.Id);
            int oldState = (int)oldDonation.Estado;
            int newState = (int)pDonation.Estado;
            if (controlStates[oldState].Exists(x => x == newState))
            {
                oldDonation.Estado = pDonation.Estado;
                this.UpdateDonation(oldDonation);
            }
            else
            {
                throw new Exception("El pasaje de estados no es válido");
            }
        }

        /// <summary>
        /// Metodo de interfaz para obtener una donacion
        /// </summary>
        /// <param name="pDonation"></param>
        /// <returns></returns>
        public Donation Get(Donation pDonation)
        {
            return this._donationRepository.Get(pDonation.Id);
        }

        /// <summary>
        /// Metodo para obtener una donacion por parte de un intermediario, controlando si el intermediario puede acceder a la misma
        /// </summary>
        /// <param name="pDonation"></param>
        /// <param name="pIntermediary"></param>
        /// <returns></returns>
        public Donation Get(Donation pDonation, Intermediary pIntermediary)
        {
            Donation donation = this._donationRepository.Get(pDonation.Id);
            if ((donation.Estado != DonationState.Cancelada) && (donation.Intermediario == pIntermediary || donation.Intermediario == null))
            {
                return donation;
            }
            throw new Exception("No se encontró la donacion");
        }

        /// <summary>
        /// Metodo para obtener una donacion por parte de una persona, controlando si la persona puede acceder a la misma
        /// </summary>
        /// <param name="pDonation"></param>
        /// <param name="pPerson"></param>
        /// <returns></returns>
        public Donation Get(Donation pDonation, Person pPerson)
        {
            Donation donation = this._donationRepository.Get(pDonation.Id);
            if (donation.Persona.Id == pPerson.Id)
            {
                return donation;
            }
            throw new Exception("No se encontró la donacion");
        }

        /// <summary>
        /// Metodo para obtener todas las donaciones
        /// </summary>
        /// <returns></returns>
        public List<Donation> GetDonations()
        {
            return this._donationRepository.GetAllList(x => x.Estado!=DonationState.Cancelada);
        }

        /// <summary>
        /// Metodo para obtener todas las donaciones registradas por una persona
        /// </summary>
        /// <param name="pPerson"></param>
        /// <returns></returns>
        public List<Donation> GetDonations(Person pPerson)
        {
            return this._donationRepository.GetAllList(x => x.Persona.Id == pPerson.Id);
        }

        /// <summary>
        /// Metodo para obtener todas las donaciones aprobadas por un intermediario
        /// </summary>
        /// <param name="pIntermediary"></param>
        /// <returns></returns>
        public List<Donation> GetDonations(Intermediary pIntermediary)
        {
            return this._donationRepository.GetAllList().FindAll(x => (x.Intermediario != null) && (x.Intermediario.Id == pIntermediary.Id) && (x.Estado != DonationState.Cancelada));
        }

        /// <summary>
        /// Metodo para registrar una donacion
        /// </summary>
        /// <param name="pDonation"></param>
        public void RegisterDonation(Donation pDonation)
        {
            this._donationRepository.Insert(pDonation);
        }

        /// <summary>
        /// Metodo para modificar una donacion
        /// </summary>
        /// <param name="pDonation"></param>
        public void UpdateDonation(Donation pDonation)
        {
            this._donationRepository.Update(pDonation);
        }
    }
}
