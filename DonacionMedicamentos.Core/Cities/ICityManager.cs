﻿using Abp.Domain.Services;
using DonacionMedicamentos.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Cities
{
    /// <summary>
    /// Interfaz del manager de la entidad Ciudad
    /// </summary>
    public interface ICityManager : IDomainService
    {
        /// <summary>
        /// Metodo de interfaz para obtener todas las ciudades de una provincia
        /// </summary>
        /// <param name="pState"></param>
        /// <returns></returns>
        List<City> GetAll(State pState);

        City Get(City pCity);
    }
}
