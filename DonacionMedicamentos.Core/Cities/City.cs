﻿using Abp.Domain.Entities;
using DonacionMedicamentos.States;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Cities
{
    /// <summary>
    /// Representa una entidad de Ciudad
    /// </summary>
    [Table("City")]
    public class City : Entity
    {
        /// <summary>
        /// Propiedad de la ciudad para guardar su ccodigo postal
        /// </summary>
        public virtual int CodigoPostal { get; set; }

        /// <summary>
        /// Propiedad de la ciudad para guardar su nombre
        /// </summary>
        public virtual string Nombre { get; set; }

        /// <summary>
        /// Propiedad de la ciudad para guardar la provincia a la que pertenece
        /// </summary>
        public virtual State Provincia { get; set; }
    }
}
