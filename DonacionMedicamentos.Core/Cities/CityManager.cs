﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DonacionMedicamentos.States;
using Abp.Domain.Repositories;

namespace DonacionMedicamentos.Cities
{
    /// <summary>
    /// Clase Manager de la entidad Ciudad
    /// </summary>
    public class CityManager : ICityManager
    {
        private readonly IRepository<City> _cityRepository;

        /// <summary>
        /// Constructor de la clase managaer de ciudad
        /// </summary>
        /// <param name="cityRepository"></param>
        public CityManager(
            IRepository<City> cityRepository)
        {
            this._cityRepository = cityRepository;
        }

        public City Get(City pCity)
        {
            return this._cityRepository.Get(pCity.Id);
        }


        /// <summary>
        /// Obtiene todas las ciudades de una determinada provincia
        /// </summary>
        /// <returns></returns>
        public List<City> GetAll(State pState)
        {
            return this._cityRepository.GetAllList(x => x.Provincia.Id == pState.Id);
        }
    }
}
