﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.States
{
    public class StateManager : IStateManager
    {
        private readonly IRepository<State> _stateRepository;

        public StateManager(IRepository<State> stateRepository)
        {
            this._stateRepository = stateRepository;
        }


        /// <summary>
        /// Obtiene todas las provincias
        /// </summary>
        /// <returns></returns>
        public List<State> GetAll()
        {
            return this._stateRepository.GetAllList();
        }

    }
}
