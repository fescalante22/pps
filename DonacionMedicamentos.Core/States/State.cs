﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace DonacionMedicamentos.States
{
    /// <summary>
    /// Representa una entidad de una provincia
    /// </summary>
    [Table("State")]
    public class State : Entity
    {
        /// <summary>
        /// Propiedad de la provincia para guardar su nombre
        /// </summary>
        public virtual string Nombre { get; set; }
    }
}