﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.States
{
    public interface IStateManager : IDomainService
    {

        List<State> GetAll();

    }
}
