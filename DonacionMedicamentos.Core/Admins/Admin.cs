﻿using Abp.Domain.Entities;
using DonacionMedicamentos.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Admins
{
    /// <summary>
    /// Representa una entidad de Administrador
    /// </summary>
    [Table("Admin")]
    public class Admin : User
    {
        /// <summary>
        /// Propiedad para el administrador para guardar su Nombre
        /// </summary>
        public virtual string Nombre { get; set; }

        /// <summary>
        /// Constructor de la clase Admin
        /// </summary>
        public Admin() : base()
        {
        }
    }
}
