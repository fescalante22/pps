﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Admins
{
    /// <summary>
    /// Clase Manager del Adminstrador
    /// </summary>
    public class AdminManager : IAdminManager
    {
        private readonly IRepository<Admin> _adminRepository;


        /// <summary>
        /// Constructor del manager del Administrador
        /// </summary>
        /// <param name="personRepository"></param>
        public AdminManager(
            IRepository<Admin> adminRepository)
        {
            this._adminRepository = adminRepository;
        }

        /// <summary>
        /// Metodo para crear un Administrador
        /// </summary>
        /// <param name="pAdmin"></param>
        public void Create(Admin pAdmin)
        {
            this._adminRepository.Insert(pAdmin);
        }

        /// <summary>
        /// Metodo para modificar un Administrador
        /// </summary>
        /// <param name="pAdmin"></param>
        public void Update(Admin pAdmin)
        {
            this._adminRepository.Update(pAdmin);
        }

        /// <summary>
        /// Metodo para obtener un administrador por su numero de cuenta
        /// </summary>
        /// <param name="pAdmin"></param>
        /// <returns></returns>
        public Admin GetByAccount(Admin pAdmin)
        {
            return this._adminRepository.Single(x => x.Account == pAdmin.Account);
        }
    }
}
