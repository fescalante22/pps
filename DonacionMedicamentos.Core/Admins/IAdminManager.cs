﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonacionMedicamentos.Admins
{
    /// <summary>
    /// Interfaz del Manager de Aministrador
    /// </summary>
    public interface IAdminManager : IDomainService
    {
        /// <summary>
        /// Metodo de interfaz para crear un Administrador
        /// </summary>
        /// <param name="pPerson"></param>
        void Create(Admin pAdmin);

        /// <summary>
        /// Metodo de interfaz para modificar un Administrador
        /// </summary>
        /// <param name="pPerson"></param>
        void Update(Admin pAdmin);

        /// <summary>
        /// Metodo de interfaz para obtener un administrador por su numero de cuenta
        /// </summary>
        /// <param name="pAdmin"></param>
        /// <returns></returns>
        Admin GetByAccount(Admin pAdmin);

    }
}
